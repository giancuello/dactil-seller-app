package com.blackfish.dactilsales;

import com.blackfish.dactilsales.util.Common;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {

        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS-05", Locale.US);
            SimpleDateFormat __format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            String date_selected = String.format("%s-%s-%s %s", 31, "01", 2020, Common.TIME_NOW());
            Date last_date = __format.parse(date_selected);
            String end_date = format.format(last_date);
            System.out.println(end_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}