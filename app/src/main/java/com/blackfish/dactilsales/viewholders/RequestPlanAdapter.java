package com.blackfish.dactilsales.viewholders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.ui.home.ui.requests.RequestsFragment;
import com.blackfish.dactilsales.util.Common;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.blackfish.dactilsales.util.Common.isUpdatedPlanRequest;

class RequestPlanViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.name_plan_req)
    TextView name_plan_req;
    @BindView(R.id.client_name)
    TextView client_name;
    @BindView(R.id.type_plan_req)
    TextView type_plan_req;
    @BindView(R.id.price_plan_req)
    TextView price_plan_req;
    @BindView(R.id.layout_state)
    LinearLayout layout_state;

    RequestPlanViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

public class RequestPlanAdapter extends RecyclerView.Adapter<RequestPlanViewHolder> {

    private Context context;
    private List<RequestReqPlanModel> requestReqPlanModelList;
    private RequestsFragment requestsFragment;

    public RequestPlanAdapter(Context context, List<RequestReqPlanModel> requestReqPlanModelList) {
        this.context = context;
        this.requestReqPlanModelList = requestReqPlanModelList;
    }

    public void setRequestsFragment(RequestsFragment requestsFragment) {
        this.requestsFragment = requestsFragment;
    }

    @NonNull
    @Override
    public RequestPlanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_request_plan_layout, parent, false);
        return new RequestPlanViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestPlanViewHolder holder, int position) {
        RequestReqPlanModel holderModel = requestReqPlanModelList.get(position);
        DecimalFormat df = new DecimalFormat("#,###.###");
        holder.client_name.setText(String.format("Nombre cliente: %s", holderModel.getClient_name()));
        holder.name_plan_req.setText(String.format("Nombre plan: %s", holderModel.getName()));
        holder.price_plan_req.setText(String.format("Precio: $%s", df.format(Double.parseDouble(holderModel.getPrice()))));
        holder.type_plan_req.setText(String.format("Tipo plan: %s", holderModel.getStatus_plan_request()));
        switch (holderModel.getStatus_plan_request()) {
            case Common.FILTER_WAIT:
                holder.layout_state.setBackgroundColor(context.getResources().getColor(R.color.stateWaiting));
                break;
            case Common.FILTER_DENIED:
                holder.layout_state.setBackgroundColor(context.getResources().getColor(R.color.stateDenied));
                break;
        }
        holder.itemView.setOnClickListener(v -> {
            isUpdatedPlanRequest = true;
            Common.reqPlanModel = holderModel;
            NavHostFragment.findNavController(requestsFragment).navigate(R.id.new_request);
        });
    }

    @Override
    public int getItemCount() {
        return requestReqPlanModelList.size();
    }
}
