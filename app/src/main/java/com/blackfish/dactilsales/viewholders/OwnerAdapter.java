package com.blackfish.dactilsales.viewholders;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.ui.home.ui.detail.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class OwnerViewholder extends RecyclerView.ViewHolder {

    @BindView(R.id.document_info)
    TextView document_info;
    @BindView(R.id.name_info)
    TextView name_info;
    @BindView(R.id.first_surname_info)
    TextView first_surname_info;
    @BindView(R.id.address_info)
    TextView address_info;
    @BindView(R.id.email_info)
    TextView email_info;

    public OwnerViewholder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

public class OwnerAdapter extends RecyclerView.Adapter<OwnerViewholder> {

    private List<OwnerModel> ownerModelList;
    private Context context;

    public OwnerAdapter(List<OwnerModel> ownerModelList, Context context) {
        this.ownerModelList = ownerModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public OwnerViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_owner_layout, parent, false);
        return new OwnerViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OwnerViewholder holder, int position) {
        OwnerModel model = ownerModelList.get(position);
        holder.document_info.setText(String.format("Documento: %s", model.getDocument()));
        holder.name_info.setText(String.format("Cliente: %s", model.getName()));
        holder.first_surname_info.setText(String.format("Apellidos: %s %s", model.getFirst_surname(), model.getSecond_surname()));
        holder.address_info.setText(String.format("Dirección: %s", model.getAddress()));
        holder.email_info.setTag(String.format("Correo: %s", (model.getEmail().trim().isEmpty()) ? "N/A" : model.getEmail()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return ownerModelList.size();
    }
}
