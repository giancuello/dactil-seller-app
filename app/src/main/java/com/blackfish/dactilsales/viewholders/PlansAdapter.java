package com.blackfish.dactilsales.viewholders;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.model.PlanModel;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.blackfish.dactilsales.util.Common.PLAN_ID;

class PlansViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.type_plan)
    TextView typePlan;
    @BindView(R.id.name_plant)
    TextView namePlan;
    @BindView(R.id.description_plant)
    TextView descriptionPlan;
    @BindView(R.id.price_plant)
    TextView pricePlan;
    @BindView(R.id.img_status)
    ImageView imgStatus;
    @BindView(R.id.card_content)
    CardView cardContent;

    PlansViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

public class PlansAdapter extends RecyclerView.Adapter<PlansViewHolder> {

    private List<PlanModel> requestPlanModelList;
    private Context context;
    private Fragment fragment;
    private ProgressDialog mDialog;

    public PlansAdapter(List<PlanModel> requestPlanModelList, Context context) {
        this.requestPlanModelList = requestPlanModelList;
        this.context = context;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public ProgressDialog getmDialog() {
        return mDialog;
    }

    @NonNull
    @Override
    public PlansViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_plans_layout, parent, false);
        return new PlansViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlansViewHolder holder, int position) {
        PlanModel modelHolder = requestPlanModelList.get(position);
        DecimalFormat df = new DecimalFormat("#,###.###");
        //Apply animation to views
        holder.imgStatus.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_anim));
        holder.cardContent.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_scale_anim));
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(60)
                .height(60)
                .endConfig()
                .buildRound(modelHolder.getName().charAt(0) + "", Color.rgb(169, 255, 150));
        holder.descriptionPlan.setText(modelHolder.getDescription());
        holder.namePlan.setText(modelHolder.getName());
        holder.pricePlan.setText(String.format("$%s", df.format(Double.parseDouble(modelHolder.getPrice().replace(".00", "")))));
        holder.typePlan.setText(modelHolder.getType_plan());
        holder.imgStatus.setImageDrawable(drawable);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog = new ProgressDialog(context);
                mDialog.setMessage("Espere...");
                mDialog.setCanceledOnTouchOutside(false);
                mDialog.show();
                PLAN_ID = modelHolder.getId();
                NavHostFragment.findNavController(fragment).navigate(R.id.navigation_owner);
            }
        });
    }

    @Override
    public int getItemCount() {
        return requestPlanModelList.size();
    }
}
