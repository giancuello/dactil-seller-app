package com.blackfish.dactilsales.util;

import android.content.Context;

public class Injected {
    public static ViewModelFactory viewModelFactory(Context context) {
        return new ViewModelFactory(context);
    }
}
