package com.blackfish.dactilsales.util;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import androidx.fragment.app.FragmentManager;

import com.blackfish.dactilsales.data.model.LoggedInUser;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.SellerToken;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class Common {
    public static final String BASE_URL = "https://api.testing.dactill.com/";
    public static final String KEY_REMEMBER = "isRememberLogin";
    public static final String KEY_LOGIN_OBJECT = "objectUser";
    public static LoggedInUser CURRENT_USER;
    public static SellerToken CURRENT_SELLER;
    public static final String FILTER_ACTIVE = "APROBADO";
    public static final String FILTER_DENIED = "RECHAZADO";
    public static final String FILTER_WAIT = "PENDIENTE";
    public static int PLAN_ID = 0;
    public static RequestReqPlanModel reqPlanModel;
    public static boolean isUpdatedPlanRequest;

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null) {
            activity.getWindow().getDecorView();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            Objects.requireNonNull(imm).hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public static String NOW(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return sdf.format(date);
    }

    public static String TIME_NOW(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.US);
        return sdf.format(date);
    }
}
