package com.blackfish.dactilsales.util;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackfish.dactilsales.data.datasource.HomeDataSource;
import com.blackfish.dactilsales.data.datasource.PlanDataSource;
import com.blackfish.dactilsales.data.datasource.OwnerDataSource;
import com.blackfish.dactilsales.data.datasource.RequestDataSource;
import com.blackfish.dactilsales.data.repository.HomeRepository;
import com.blackfish.dactilsales.data.repository.LoginRepository;
import com.blackfish.dactilsales.data.datasource.LoginDataSource;
import com.blackfish.dactilsales.data.repository.NewRequestRepository;
import com.blackfish.dactilsales.data.repository.OwnerRepository;
import com.blackfish.dactilsales.data.repository.RequestRepository;
import com.blackfish.dactilsales.ui.home.ui.createplan.CreatePlanViewModel;
import com.blackfish.dactilsales.ui.home.ui.home.HomeViewModel;
import com.blackfish.dactilsales.ui.home.ui.newrequest.NewRequestViewModel;
import com.blackfish.dactilsales.ui.home.ui.owner.OwnerViewModel;
import com.blackfish.dactilsales.ui.home.ui.requests.RequestsViewModel;
import com.blackfish.dactilsales.ui.home.ui.tracing.TracingViewModel;
import com.blackfish.dactilsales.ui.login.LoginViewModel;

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
public class ViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    private Context context;
    public ViewModelFactory(@NonNull Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(LoginRepository.getInstance(new LoginDataSource()), context);
        } else if (modelClass.isAssignableFrom(HomeViewModel.class)){
            return (T) new HomeViewModel(context, HomeRepository.getInstance(new HomeDataSource()));
        } else if (modelClass.isAssignableFrom(NewRequestViewModel.class)){
            return (T) new NewRequestViewModel(context, NewRequestRepository.getInstance(new PlanDataSource()));
        } else if (modelClass.isAssignableFrom(RequestsViewModel.class)){
            return (T) new RequestsViewModel(context, RequestRepository.getInstance(new RequestDataSource()));
        } else if (modelClass.isAssignableFrom(TracingViewModel.class)){
            return (T) new TracingViewModel(context);
        } else if (modelClass.isAssignableFrom(OwnerViewModel.class)){
            return (T) new OwnerViewModel(context, OwnerRepository.getInstance(new OwnerDataSource()));
        } else if (modelClass.isAssignableFrom(CreatePlanViewModel.class)){
            return (T) new CreatePlanViewModel(context, RequestRepository.getInstance(new RequestDataSource()));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
