package com.blackfish.dactilsales.ui.home.ui.requests;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;
import com.blackfish.dactilsales.data.repository.RequestRepository;
import com.blackfish.dactilsales.util.Common;
import com.blackfish.dactilsales.viewholders.RequestPlanAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RequestsViewModel extends ViewModel {

    private MutableLiveData<Integer> mText;
    private MutableLiveData<RequestPlanAdapter> mAdapterObserver;
    private RequestRepository requestRepository;
    @NonNull
    private Context context;
    public RequestsViewModel(@NonNull Context context, RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
        this.context = context;
        mText = new MutableLiveData<>();
        mAdapterObserver = new MutableLiveData<>();
    }

    public LiveData<RequestPlanAdapter> getAdapter(){
        return mAdapterObserver;
    }

    public LiveData<Integer> getText() {
        return mText;
    }

    private void attachListToAdapter(List<RequestReqPlanModel> requestReqPlanModel){
        List<RequestReqPlanModel> clearRequestReqPlanModel = new ArrayList<>();
        Log.d(getClass().getName(), Arrays.toString(requestReqPlanModel.toArray()));
        for(RequestReqPlanModel pm : requestReqPlanModel){
            if (!pm.getStatus_plan_request().equals(Common.FILTER_ACTIVE)){
                clearRequestReqPlanModel.add(pm);
                Log.d(getClass().getName(), pm.toString());
            }

        }
        RequestPlanAdapter adapter = new RequestPlanAdapter(context, clearRequestReqPlanModel);
        mAdapterObserver.setValue(adapter);
    }

    public void getList(){
        Result<ResponseRequestModelList> modelList = requestRepository.getListModel();
        if (modelList instanceof Result.Success) {
            ResponseRequestModelList data = ((Result.Success<ResponseRequestModelList>) modelList).getData();
            if (data.getCode() == 1 && data.getRecordsFiltered() > 0) {
                attachListToAdapter(data.getData());
            } else {
                mText.setValue(R.string.empty_source_request_string);
            }
        }
    }
}