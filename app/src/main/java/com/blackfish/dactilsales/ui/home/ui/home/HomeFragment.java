package com.blackfish.dactilsales.ui.home.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.ui.login.LoginActivity;
import com.blackfish.dactilsales.util.Common;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.blackfish.dactilsales.viewholders.PlansAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;

public class HomeFragment extends Fragment {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.plans_recycler)
    RecyclerView planRecycler;
    @BindView(R.id.empty_source)
    TextView empty_source;

    private HomeViewModel homeViewModel;
    private PlansAdapter mAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ViewModelFactory viewModelFactory = Injected.viewModelFactory(getContext());
        homeViewModel =
                new ViewModelProvider(this, viewModelFactory).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, root);
        homeViewModel.setFragment(this);
        homeViewModel.getAdapter().observe(getViewLifecycleOwner(), new Observer<PlansAdapter>() {
            @Override
            public void onChanged(PlansAdapter plansAdapter) {
                Log.d(getClass().getName(), "Adapter is change");
                swipeRefresh.setVisibility(View.VISIBLE);
                empty_source.setVisibility(View.GONE);
                planRecycler.setAdapter(plansAdapter);
                plansAdapter.notifyDataSetChanged();
                swipeRefresh.setRefreshing(false);
                mAdapter = plansAdapter;
            }
        });
        homeViewModel.getEmptySource().observe(getViewLifecycleOwner(), this::displayEmptySource);
        planRecycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        planRecycler.setLayoutManager(layoutManager);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(this::attachRecycler);
        swipeRefresh.post(this::attachRecycler);
        setHasOptionsMenu(true);
        return root;
    }

    private void displayEmptySource(Integer integer) {
        swipeRefresh.setRefreshing(false);
        swipeRefresh.setVisibility(View.GONE);
        empty_source.setVisibility(View.VISIBLE);
        empty_source.setText(getString(integer));
    }

    private void attachRecycler() {
        swipeRefresh.setRefreshing(true);
        homeViewModel.setPlanList();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.add_plan) {
            if (Common.CURRENT_SELLER.getSellerModel().getType_seller().equals("MASTER CHIEF")
                    || Common.CURRENT_SELLER.getSellerModel().getType_seller().equals("SUPERVISOR")) {
                NavHostFragment.findNavController(this).navigate(R.id.create_plan);
            } else {
                NavHostFragment.findNavController(this).navigate(R.id.new_request);
            }
        }else if (item.getItemId() == R.id.exit_session){
            new AlertDialog.Builder(getContext())
                    .setTitle("¿Listo para abandonar?")
                    .setMessage("¿Desea cerrar sesión en este dispositivo?")
                    .setPositiveButton("Cerrar", (dialog, which) -> {
                        Paper.book().destroy();
                        getActivity().startActivity(new Intent(getContext(), LoginActivity.class));
                        getActivity().finishAffinity();
                    }).setNegativeButton("Cancelar",
                    (dialog, which) -> dialog.dismiss())
                    .create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mAdapter.getmDialog() != null && mAdapter.getmDialog().isShowing()){
            mAdapter.getmDialog().dismiss();
            mAdapter.getmDialog().cancel();
        }
    }
}