package com.blackfish.dactilsales.ui.home.ui.newrequest;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.SellerToken;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModel;
import com.blackfish.dactilsales.data.repository.NewRequestRepository;

import io.paperdb.Paper;

import static com.blackfish.dactilsales.util.Common.KEY_LOGIN_OBJECT;

public class NewRequestViewModel extends ViewModel {
    @NonNull
    private Context context;
    private NewRequestRepository newRequestRepository;
    private MutableLiveData<RequestPlanResult> planResult = new MutableLiveData<>();
    private SellerToken sellerToken;

    public NewRequestViewModel(@NonNull Context context, NewRequestRepository newRequestRepository) {
        Paper.init(context);
        this.context = context;
        this.newRequestRepository = newRequestRepository;
        sellerToken = Paper.book().read(KEY_LOGIN_OBJECT);
    }

    public LiveData<RequestPlanResult> getResult() {
        return planResult;
    }

    public void sendRequest(RequestReqPlanModel requestReqPlanModel) {
        // can be launched in a separate asynchronous job
        Result<ResponseRequestModel> result = newRequestRepository.requestPlan(requestReqPlanModel, sellerToken.getData());
        if (result instanceof Result.Success) {
            ResponseRequestModel data = ((Result.Success<ResponseRequestModel>) result).getData();((Result.Success<ResponseRequestModel>) result).getData();
            planResult.setValue(new RequestPlanResult(data.getData()));
        } else {
            planResult.setValue(new RequestPlanResult(R.string.request_failed));
        }
    }

    public void updateSendRequest(RequestReqPlanModel requestReqPlanModel) {
        // can be launched in a separate asynchronous job
        Result<ResponseRequestModel> result = newRequestRepository.updateRequestPlan(requestReqPlanModel, sellerToken.getData());
        if (result instanceof Result.Success) {
            ResponseRequestModel data = ((Result.Success<ResponseRequestModel>) result).getData();((Result.Success<ResponseRequestModel>) result).getData();
            planResult.setValue(new RequestPlanResult(data.getData()));
        } else {
            planResult.setValue(new RequestPlanResult(R.string.request_failed));
        }
    }
}
