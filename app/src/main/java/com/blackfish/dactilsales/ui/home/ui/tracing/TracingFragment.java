package com.blackfish.dactilsales.ui.home.ui.tracing;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseOwnerModel;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.blackfish.dactilsales.viewholders.OwnerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class TracingFragment extends Fragment {

    private TracingViewModel tracingViewModel;
    private OwnerAdapter ownerAdapter;
    private List<OwnerModel> ownerModelList;
    @BindView(R.id.swipeOwner)
    SwipeRefreshLayout swipeOwner;
    @BindView(R.id.recyclerOwner)
    RecyclerView recyclerOwner;

    private long delay = 1000;
    private long editTextLastWord = 0;
    public SearchView searchView;
    Handler handler = new Handler();
    private boolean onResumingChanged;

    private String search = "";

    private Runnable runnable = new Runnable() {
        public void run() {
            if (!onResumingChanged) {
                if (System.currentTimeMillis() > (editTextLastWord + delay - 500)) {
                    swipeOwner.setRefreshing(true);
                    if (search.equals("")){
                        attachRecycler();
                    }else{
                        ResponseOwnerModel ownerModel = tracingViewModel.searchFilter(search);
                        addRecycler(ownerModel);
                    }
                }
            }
        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ViewModelFactory viewModelFactory = Injected.viewModelFactory(getContext());
        tracingViewModel =
                new ViewModelProvider(this, viewModelFactory).get(TracingViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tracing, container, false);
        ButterKnife.bind(this, root);
        recyclerOwner.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerOwner.setLayoutManager(layoutManager);
        swipeOwner.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.colorWhite);
        swipeOwner.setOnRefreshListener(this::attachRecycler);
        swipeOwner.post(this::attachRecycler);
        setHasOptionsMenu(true);
        return root;
    }

    private void attachRecycler() {
        ResponseOwnerModel responseOwnerModel = tracingViewModel.getOwners();
        addRecycler(responseOwnerModel);
    }

    private void addRecycler(ResponseOwnerModel responseOwnerModel) {
        if (responseOwnerModel != null && responseOwnerModel.getData() != null) {
            ownerModelList = new ArrayList<>(responseOwnerModel.getData());
            ownerAdapter = new OwnerAdapter(ownerModelList, getContext());
            recyclerOwner.setAdapter(ownerAdapter);
            ownerAdapter.notifyDataSetChanged();
        }
        swipeOwner.setRefreshing(false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_tracing, menu);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        EditText searchEditText = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.colorWhite));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorWhite));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d("TAG onQueryTextSubmit", s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d("TAG onQueryTextChange", s);
                onResumingChanged = false;
                search = s;
                handler.removeCallbacks(runnable);
                editTextLastWord = System.currentTimeMillis();
                handler.postDelayed(runnable, delay);
                return false;
            }
        });
    }


}