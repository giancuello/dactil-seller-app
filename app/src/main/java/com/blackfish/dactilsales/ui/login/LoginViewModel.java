package com.blackfish.dactilsales.ui.login;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.repository.LoginRepository;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.model.SellerToken;

import io.paperdb.Paper;

import static com.blackfish.dactilsales.util.Common.CURRENT_SELLER;
import static com.blackfish.dactilsales.util.Common.CURRENT_USER;
import static com.blackfish.dactilsales.util.Common.KEY_LOGIN_OBJECT;

public class LoginViewModel extends ViewModel {

    private final String TAG = getClass().getName();
    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    public LoginViewModel(LoginRepository loginRepository, @NonNull Context context) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(String username, String password) {
        // can be launched in a separate asynchronous job
        Result<SellerToken> result = loginRepository.login(username, password);

        if (result instanceof Result.Success) {
            SellerToken data = ((Result.Success<SellerToken>) result).getData();
            Paper.book().write(KEY_LOGIN_OBJECT, data);
            CURRENT_SELLER = data;
            loginResult.setValue(new LoginResult(new LoggedInUserView(data.getUser().getUsername())));
        } else {
            loginResult.setValue(new LoginResult(R.string.login_failed));
        }
    }

    public void isLoginRemembered() {
        try {
            SellerToken data = Paper.book().read(KEY_LOGIN_OBJECT);
            login(data.getUser().getUsername(), data.getUser().getPassword());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    /*public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }*/

    // A placeholder username validation check
    /*private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        return !username.trim().isEmpty();
    }*/

    // A placeholder password validation check
    /*private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }*/
}
