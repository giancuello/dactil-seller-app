package com.blackfish.dactilsales.ui.home.ui.owner;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.model.CityModel;
import com.blackfish.dactilsales.data.model.CountryModel;
import com.blackfish.dactilsales.data.model.DepartmentModel;
import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.data.model.TypeDocumentModel;
import com.blackfish.dactilsales.util.Common;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.blackfish.dactilsales.util.Common.PLAN_ID;

public class OwnerFragment extends Fragment {

    @BindView(R.id.doc_type)
    MaterialSpinner docType;
    @BindView(R.id.document_owner_layout)
    TextInputLayout document_owner_layout;
    @BindView(R.id.document_input_owner)
    TextInputEditText document_input_owner;
    @BindView(R.id.name_owner_layout)
    TextInputLayout name_owner_layout;
    @BindView(R.id.name_owner_input)
    TextInputEditText name_owner_input;
    @BindView(R.id.first_name_layout)
    TextInputLayout first_name_layout;
    @BindView(R.id.first_name_input)
    TextInputEditText first_name_input;
    @BindView(R.id.second_name_layout)
    TextInputLayout second_name_layout;
    @BindView(R.id.second_name_input)
    TextInputEditText second_name_input;
    @BindView(R.id.city)
    MaterialSpinner city;
    @BindView(R.id.country)
    MaterialSpinner country;
    @BindView(R.id.address_layout)
    TextInputLayout address_layout;
    @BindView(R.id.address_input)
    TextInputEditText address_input;
    @BindView(R.id.department)
    MaterialSpinner department;
    @BindView(R.id.phone_1_layout)
    TextInputLayout phone_1_layout;
    @BindView(R.id.phone_1_input)
    TextInputEditText phone_1_input;
    @BindView(R.id.phone_2_layout)
    TextInputLayout phone_2_layout;
    @BindView(R.id.phone_2_input)
    TextInputEditText phone_2_input;
    @BindView(R.id.email_layout)
    TextInputLayout email_layout;
    @BindView(R.id.email_input)
    TextInputEditText email_input;
    @BindView(R.id.username_layout)
    TextInputLayout username_layout;
    @BindView(R.id.username_input)
    TextInputEditText username_input;
    @BindView(R.id.password_layout)
    TextInputLayout password_layout;
    @BindView(R.id.password_input)
    TextInputEditText password_input;
    @BindView(R.id.repeat_password_layout)
    TextInputLayout repeat_password_layout;
    @BindView(R.id.repeat_password_input)
    TextInputEditText repeat_password_input;
    @BindView(R.id.register_owner)
    Button register_owner;

    private OwnerViewModel mViewModel;
    private boolean passwordIsCorrect = false;
    private List<String> dataSplitCity = new ArrayList<>();
    private int selectedIdCity;
    private List<String> dataSplitCountry = new ArrayList<>();
    private int selectedIdCountry;
    private List<String> dataSplitDepartment = new ArrayList<>();
    private int selectedIdDepartment;
    private List<String> dataSplitTypeDocument = new ArrayList<>();
    private int selectedIdTypeDocument;
    private boolean isValidUsername;
    private Handler handler = new Handler();
    private boolean onResumingChanged;
    private long editTextLastWord = 0;
    private String search = "";
    private Runnable runnable = new Runnable() {
        public void run() {
            if (!onResumingChanged) {
                if (System.currentTimeMillis() > (editTextLastWord + 1000 - 500)) {
                    mViewModel.searchIfExist(search);
                }
            }
        }
    };

    public static OwnerFragment newInstance() {
        return new OwnerFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.owner_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ViewModelFactory injected = Injected.viewModelFactory(getContext());
        mViewModel = new ViewModelProvider(this, injected).get(OwnerViewModel.class);
        mViewModel.getCityList();
        mViewModel.getCountryList();
        mViewModel.getDepartmentList();
        mViewModel.getTypeDocumentList();

        mViewModel.getObserverCityModel().observe(getViewLifecycleOwner(), cityModels -> {
            List<String> cModels = new ArrayList<>();
            for (CityModel cm : cityModels) {
                cModels.add(cm.getName());
                dataSplitCity.add(String.format("%s_%s", cm.getId(), cm.getName()));
            }
            getSelectedCity(cModels.get(0));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, cModels);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            city.setAdapter(adapter);
        });
        mViewModel.getObserverCountryModel().observe(getViewLifecycleOwner(), countryModels -> {
            List<String> cModels = new ArrayList<>();
            for (CountryModel cm : countryModels) {
                cModels.add(cm.getName());
                dataSplitCountry.add(String.format("%s_%s", cm.getId(), cm.getName()));
            }
            getSelectedCountry(cModels.get(0));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, cModels);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            country.setAdapter(adapter);
        });
        mViewModel.getObserverDepartmentModel().observe(getViewLifecycleOwner(), departmentModels -> {
            List<String> cModels = new ArrayList<>();
            for (DepartmentModel dm : departmentModels) {
                cModels.add(dm.getName());
                dataSplitDepartment.add(String.format("%s_%s", dm.getId(), dm.getName()));
            }
            getSelectedDepartment(cModels.get(0));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, cModels);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            department.setAdapter(adapter);
        });
        mViewModel.getObserverTypeDocumentModel().observe(getViewLifecycleOwner(), typeDocumentModels -> {
            List<String> cModels = new ArrayList<>();
            for (TypeDocumentModel tdm : typeDocumentModels) {
                cModels.add(tdm.getName());
                dataSplitTypeDocument.add(String.format("%s_%s", tdm.getId(), tdm.getName()));
            }
            getSelectedTypeDocument(cModels.get(0));//Default
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, cModels);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            docType.setAdapter(adapter);
        });

        mViewModel.getResultSave().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s.equals("Error al intentar registrar el Propietario.")) {
                    Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Registro de propietario")
                            .setMessage(s)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    NavHostFragment.findNavController(OwnerFragment.this).navigateUp();
                                }
                            }).create().show();
                }
            }
        });

        docType.setOnItemSelectedListener((view, position, id, item) -> getSelectedTypeDocument(item));

        country.setOnItemSelectedListener((view, position, id, item) -> getSelectedCountry(item));

        department.setOnItemSelectedListener((view, position, id, item) -> getSelectedDepartment(item));

        city.setOnItemSelectedListener((view, position, id, item) -> getSelectedCity(item));

        username_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    onResumingChanged = false;
                    search = s.toString();
                    handler.removeCallbacks(runnable);
                    editTextLastWord = System.currentTimeMillis();
                    handler.postDelayed(runnable, 1000);
                }
            }
        });

        mViewModel.getRsult().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s.equals("1")) {
                    username_layout.setError(null);
                    isValidUsername = true;
                } else {
                    username_layout.setError(s);
                    isValidUsername = false;
                }
            }
        });

        repeat_password_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = password_input.getText().toString();
                if (!text.equals(s.toString())) {
                    repeat_password_layout.setError("Las contraseñas no coinciden.");
                    passwordIsCorrect = false;
                } else {
                    repeat_password_layout.setError(null);
                    passwordIsCorrect = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getSelectedCity(Object item) {
        for (String d : dataSplitCity) {
            String object = (String) item;
            String[] dataSplit = d.split("_");
            if (dataSplit[1].contains(object)) {
                selectedIdCity = Integer.parseInt(dataSplit[0]);
            }
        }
    }

    private void getSelectedDepartment(Object item) {
        for (String d : dataSplitDepartment) {
            String object = (String) item;
            String[] dataSplit = d.split("_");
            if (dataSplit[1].contains(object)) {
                selectedIdDepartment = Integer.parseInt(dataSplit[0]);
            }
        }
    }

    private void getSelectedCountry(Object item) {
        for (String d : dataSplitCountry) {
            String object = (String) item;
            String[] dataSplit = d.split("_");
            if (dataSplit[1].contains(object)) {
                selectedIdCountry = Integer.parseInt(dataSplit[0]);
            }
        }
    }

    private void getSelectedTypeDocument(Object item) {
        for (String d : dataSplitTypeDocument) {
            String object = (String) item;
            String[] dataSplit = d.split("_");
            if (dataSplit[1].contains(object)) {
                selectedIdTypeDocument = Integer.parseInt(dataSplit[0]);
            }
        }
    }

    @OnClick(R.id.register_owner)
    void registerOwner() {
        if (checkForm()) {
            OwnerModel ownerModel = new OwnerModel();
            ownerModel.setAddress(address_input.getText().toString());
            ownerModel.setCity(selectedIdCity);
            ownerModel.setCountry(selectedIdCountry);
            ownerModel.setDepartament(selectedIdDepartment);
            ownerModel.setDocument(document_input_owner.getText().toString());
            ownerModel.setEmail(email_input.getText().toString());
            ownerModel.setFirst_surname(first_name_input.getText().toString());
            ownerModel.setName(name_owner_input.getText().toString());
            ownerModel.setPhone1(phone_1_input.getText().toString());
            ownerModel.setPassword(password_input.getText().toString());
            ownerModel.setPhone2(phone_2_input.getText().toString());
            ownerModel.setSecond_surname(second_name_input.getText().toString());
            ownerModel.setType_document(selectedIdTypeDocument);
            ownerModel.setUsername(username_input.getText().toString());
            List<Integer> plansId = new ArrayList<>();
            if (PLAN_ID != 0) {
                plansId.add(PLAN_ID);
            }
            ownerModel.setPlan(plansId);
            mViewModel.createOwner(ownerModel);
        }
    }

    private boolean checkForm() {
        boolean[] checker = new boolean[8];
        if (document_input_owner.getText().toString().isEmpty()) {
            document_owner_layout.setError("El campo de documento no puede estar vacío.");
            checker[0] = false;
        } else {
            checker[0] = true;
            document_owner_layout.setError(null);
        }
        if (name_owner_input.getText().toString().isEmpty()) {
            name_owner_layout.setError("El campo de nombre no puede estar vacío.");
            checker[1] = false;
        } else {
            checker[1] = true;
            name_owner_layout.setError(null);
        }
        if (first_name_input.getText().toString().isEmpty()) {
            first_name_layout.setError("El campo de Primer Apellido no puede estar vacío.");
            checker[2] = false;
        } else {
            checker[2] = true;
            first_name_layout.setError(null);
        }
        if (second_name_input.getText().toString().isEmpty()) {
            second_name_layout.setError("El campo de Segundo Apellido no puede estar vacío.");
            checker[3] = false;
        } else {
            second_name_layout.setError(null);
            checker[3] = true;
        }
        if (address_input.getText().toString().isEmpty()) {
            address_layout.setError("El campo de dirección no puede estar vacío.");
            checker[4] = false;
        } else {
            checker[4] = true;
            address_layout.setError(null);
        }
        if (phone_1_input.getText().toString().isEmpty()) {
            phone_1_layout.setError("El campo de Telefono no puede estar vacío.");
            checker[5] = false;
        } else {
            checker[5] = true;
            phone_1_layout.setError(null);
        }
        if (email_input.getText().toString().isEmpty()) {
            checker[6] = false;
            email_layout.setError("El campo de Correo no puede estar vacío.");
        } else {
            checker[6] = true;
            email_layout.setError(null);
        }
        if (password_input.getText().toString().isEmpty()) {
            checker[7] = false;
            password_layout.setError("El campo de contraseña no puede estar vacío.");
        } else {
            checker[7] = true;
            password_layout.setError(null);
        }
        if (!passwordIsCorrect) {
            repeat_password_layout.setError("Las contraseñas no coinciden.");
        } else {
            repeat_password_layout.setError(null);
        }
        if (!isValidUsername) {
            username_layout.setError("Este nombre de usuario ya existe.");
        } else {
            username_layout.setError(null);
        }
        return checker[0] && checker[1] && checker[2] && checker[3] && checker[4] && checker[5] && checker[6] && checker[7] && passwordIsCorrect && isValidUsername;
    }

    @Override
    public void onResume() {
        super.onResume();
        search = "";
        onResumingChanged = true;
    }
}
