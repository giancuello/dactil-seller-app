package com.blackfish.dactilsales.ui.home.ui.createplan;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.components.DatePickerFragment;
import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.blackfish.dactilsales.util.Common.CURRENT_SELLER;
import static com.blackfish.dactilsales.util.Common.TIME_NOW;

public class CreatePlanFragment extends Fragment implements DatePicker.OnDateChangedListener {

    private static String DATE_FORMAT_END = "";
    private static String DATE_FORMAT_START = "";
    @BindView(R.id.name_plan_layout)
    TextInputLayout nameLayout;
    @BindView(R.id.description_layout)
    TextInputLayout descriptionLayout;
    @BindView(R.id.price_layout)
    TextInputLayout priceLayout;
    @BindView(R.id.start_date_layout)
    TextInputLayout startDateLayout;
    @BindView(R.id.end_date_layout)
    TextInputLayout endDateLayout;
    @BindView(R.id.bussines_number_layout)
    TextInputLayout numberBussinesLayout;
    @BindView(R.id.user_number_per_bussines_layout)
    TextInputLayout userBussinesLayout;
    @BindView(R.id.cashier_number_per_bussines_layout)
    TextInputLayout cashierLayout;
    @BindView(R.id.points_number_per_bussines_layout)
    TextInputLayout pointsLayout;
    @BindView(R.id.counter_number_per_bussines_layout)
    TextInputLayout counter_number_per_bussines_layout;
    @BindView(R.id.warehouse_number_per_bussines_layout)
    TextInputLayout warehouse_number_per_bussines_layout;
    //Inputs
    @BindView(R.id.plan_type)
    MaterialSpinner planType;
    @BindView(R.id.name_input)
    TextInputEditText nameInput;
    @BindView(R.id.description_input)
    TextInputEditText descriptionInput;
    @BindView(R.id.price_input)
    TextInputEditText priceInput;
    @BindView(R.id.start_date_input)
    TextInputEditText startDateInput;
    @BindView(R.id.end_date_input)
    TextInputEditText endDateInput;
    @BindView(R.id.bussines_number_input)
    TextInputEditText numberBussinesInput;
    @BindView(R.id.user_number_per_bussines_input)
    TextInputEditText userBussinesInput;
    @BindView(R.id.cashier_number_per_bussines_input)
    TextInputEditText cashierInput;
    @BindView(R.id.points_number_per_bussines_input)
    TextInputEditText pointsInput;
    @BindView(R.id.register_plan)
    Button registerPlan;
    @BindView(R.id.counter_number_per_bussines_input)
    TextInputEditText counter_number_per_bussines_input;
    @BindView(R.id.warehouse_number_per_bussines_input)
    TextInputEditText warehouse_number_per_bussines_input;
    @BindView(R.id.isInventory)
    CheckBox isInventory;
    @BindView(R.id.isMultipleCloseCash)
    CheckBox isMultipleCloseCash;
    @BindView(R.id.isControlInventory)
    CheckBox isControlInventory;
    @BindView(R.id.isCombinedPromotion)
    CheckBox isCombinedPromotion;
    @BindView(R.id.isDashboard)
    CheckBox isDashboard;
    @BindView(R.id.isReportExport)
    CheckBox isReportExport;
    @BindView(R.id.isModulePermissions)
    CheckBox isModulePermissions;

    private String typePlanString;
    private CreatePlanViewModel mViewModel;
    private DatePickerFragment mPicker;
    private int stateDate;

    public static CreatePlanFragment newInstance() {
        return new CreatePlanFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.create_plan_fragment, container, false);
        ButterKnife.bind(this, root);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.plan_array, android.R.layout.simple_spinner_item);
        planType.setAdapter(adapter);
        setHasOptionsMenu(true);
        planType.setOnItemSelectedListener(
                (view, position, id, item) ->
                        typePlanString = (String) item);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelFactory viewModelFactory = Injected.viewModelFactory(getContext());
        mViewModel = new ViewModelProvider(this, viewModelFactory).get(CreatePlanViewModel.class);
        mViewModel.getDataResult().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String integer) {
                new AlertDialog.Builder(getActivity())
                        .setMessage(integer)
                        .setPositiveButton("Aceptar", (dialog, which) -> {
                            dialog.dismiss();
                            NavHostFragment.findNavController(CreatePlanFragment.this).navigateUp();
                        }).create().show();
            }
        });
    }

    @SuppressWarnings("ConstantConditions")
    @OnClick(R.id.register_plan)
    void registerPlan(View view) {
        if (checkForm()) {
            PlanModel pm = new PlanModel();
            pm.setCreated_by(CURRENT_SELLER.getSellerModel().getId());
            pm.setPoints_of_sale_number(Integer.parseInt(pointsInput.getText().toString()));
            pm.setCashier_number_per_pdv(Integer.parseInt(cashierInput.getText().toString()));
            pm.setUser_number_per_pdv(Integer.parseInt(userBussinesInput.getText().toString()));
            pm.setBussines_number(Integer.parseInt(numberBussinesInput.getText().toString()));
            pm.setStatus(true);
            pm.setStart_date(DATE_FORMAT_START);
            pm.setEnd_date(DATE_FORMAT_END);
            pm.setDescription(descriptionInput.getText().toString());
            pm.setName(nameInput.getText().toString());
            pm.setPrice(priceInput.getText().toString());
            pm.setType_plan(typePlanString);
            pm.setSupplies_inventory(isInventory.isChecked());
            pm.setMultiple_closer_cashier(isMultipleCloseCash.isChecked());
            pm.setInventory_control(isControlInventory.isChecked());
            pm.setCombo_promotion_create(isCombinedPromotion.isChecked());
            pm.setDashboard(isDashboard.isChecked());
            pm.setExport_report(isReportExport.isChecked());
            pm.setRole_andpermission_module(isModulePermissions.isChecked());
            mViewModel.createPlan(pm);
        }
    }

    @OnClick(R.id.image_selected_date)
    void startDateSelected() {
        stateDate = 1;
        mPicker = new DatePickerFragment(this);
        mPicker.show(getParentFragmentManager(), "Fecha de inicio");
    }

    @OnClick(R.id.date_selected_end)
    void endDateSelected() {
        stateDate = 2;
        mPicker = new DatePickerFragment(this);
        mPicker.show(getParentFragmentManager(), "Fecha de finalización");
    }

    @SuppressWarnings("ConstantConditions")
    private boolean checkForm() {
        boolean[] checker = new boolean[9];
        if (nameInput.getText().toString().isEmpty()) {
            nameLayout.setError("El campo nombre no puede estar vacío.");
            checker[0] = false;
        } else {
            nameLayout.setError(null);
            checker[0] = true;
        }
        if (descriptionInput.getText().toString().isEmpty()) {
            descriptionLayout.setError("El campo descripción no puede estar vacío.");
            checker[1] = false;
        } else {
            checker[1] = true;
            descriptionLayout.setError(null);
        }
        if (priceInput.getText().toString().isEmpty()) {
            checker[2] = false;
            priceLayout.setError("El campo precio no puede estar vacío.");
        } else {
            checker[2] = true;
            priceLayout.setError(null);
        }
        if (numberBussinesInput.getText().toString().isEmpty()) {
            checker[3] = false;
            numberBussinesLayout.setError("El campo #Negocios no puede estar vacío.");
        } else {
            checker[3] = true;
            numberBussinesLayout.setError(null);
        }
        if (userBussinesInput.getText().toString().isEmpty()) {
            checker[4] = false;
            userBussinesLayout.setError("El campo #Usuarios no puede estar vacío.");
        } else {
            checker[4] = true;
            userBussinesLayout.setError(null);
        }
        if (cashierInput.getText().toString().isEmpty()) {
            checker[5] = false;
            cashierLayout.setError("El campo #Cajeros no puede estar vacío.");
        } else {
            checker[5] = true;
            cashierLayout.setError(null);
        }
        if (pointsInput.getText().toString().isEmpty()) {
            checker[6] = false;
            pointsLayout.setError("El campo #Puntos de venta no puede estar vacío.");
        } else {
            checker[6] = true;
            pointsLayout.setError(null);
        }
        if (counter_number_per_bussines_input.getText().toString().isEmpty()) {
            checker[7] = false;
            counter_number_per_bussines_layout.setError("El campo #Contador por puntos de venta no puede estar vacío.");
        } else {
            checker[7] = true;
            counter_number_per_bussines_layout.setError(null);
        }
        if (warehouse_number_per_bussines_input.getText().toString().isEmpty()) {
            checker[8] = false;
            warehouse_number_per_bussines_layout.setError("El campo #Bodegas por puntos de venta no puede estar vacío.");
        } else {
            checker[8] = true;
            warehouse_number_per_bussines_layout.setError(null);
        }
        return checker[0] && checker[1] && checker[2] && checker[3] && checker[4] && checker[5] && checker[6] && checker[7] && checker[8];
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS-05", Locale.US);
        SimpleDateFormat __format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String date_selected;
        String formatter_date;
        if (monthOfYear <= 9)
            date_selected = String.format("%s-0%s-%s %s", year, monthOfYear + 1, dayOfMonth, TIME_NOW());
        else
            date_selected = String.format("%s-%s-%s %s", year, monthOfYear + 1, dayOfMonth, TIME_NOW());
        try {
            Date last_date = __format.parse(date_selected);
            formatter_date = format.format(last_date);
        } catch (Exception e) {
            formatter_date = date_selected;
        }
        if (stateDate == 1) {
            startDateInput.setText(date_selected);
            DATE_FORMAT_START = formatter_date;
        }
        if (stateDate == 2) {
            endDateInput.setText(date_selected);
            DATE_FORMAT_END = formatter_date;
        }
    }
}
