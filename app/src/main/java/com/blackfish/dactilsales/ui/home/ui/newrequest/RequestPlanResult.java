package com.blackfish.dactilsales.ui.home.ui.newrequest;

import androidx.annotation.Nullable;

import com.blackfish.dactilsales.data.model.RequestReqPlanModel;

public class RequestPlanResult {
    @Nullable
    private RequestReqPlanModel success;
    @Nullable
    private Integer error;

    RequestPlanResult(@Nullable Integer error) {
        this.error = error;
    }

    RequestPlanResult(@Nullable RequestReqPlanModel success) {
        this.success = success;
    }

    @Nullable
    RequestReqPlanModel getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }
}
