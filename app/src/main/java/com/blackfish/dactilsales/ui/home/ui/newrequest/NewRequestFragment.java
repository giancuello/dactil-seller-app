package com.blackfish.dactilsales.ui.home.ui.newrequest;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.components.DatePickerFragment;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.util.Common;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.blackfish.dactilsales.util.Common.CURRENT_SELLER;
import static com.blackfish.dactilsales.util.Common.NOW;
import static com.blackfish.dactilsales.util.Common.TIME_NOW;
import static com.blackfish.dactilsales.util.Common.isUpdatedPlanRequest;
import static com.blackfish.dactilsales.util.Common.reqPlanModel;

public class NewRequestFragment extends Fragment implements DatePicker.OnDateChangedListener {

    @BindView(R.id.name_plan_layout)
    TextInputLayout nameLayout;
    @BindView(R.id.client_name_layout)
    TextInputLayout clientLayout;
    @BindView(R.id.phone_client_layout)
    TextInputLayout phoneLayout;
    @BindView(R.id.address_client_layout)
    TextInputLayout addressLayout;
    @BindView(R.id.description_layout)
    TextInputLayout descriptionLayout;
    @BindView(R.id.price_layout)
    TextInputLayout priceLayout;
    @BindView(R.id.start_date_layout)
    TextInputLayout startDateLayout;
    @BindView(R.id.end_date_layout)
    TextInputLayout endDateLayout;
    @BindView(R.id.bussines_number_layout)
    TextInputLayout numberBussinesLayout;
    @BindView(R.id.user_number_per_bussines_layout)
    TextInputLayout userBussinesLayout;
    @BindView(R.id.cashier_number_per_bussines_layout)
    TextInputLayout cashierLayout;
    @BindView(R.id.points_number_per_bussines_layout)
    TextInputLayout pointsLayout;
    @BindView(R.id.counter_number_per_bussines_layout)
    TextInputLayout counter_number_per_bussines_layout;
    @BindView(R.id.warehouse_number_per_bussines_layout)
    TextInputLayout warehouse_number_per_bussines_layout;
    //Inputs
    @BindView(R.id.name_input)
    TextInputEditText nameInput;
    @BindView(R.id.client_name_input)
    TextInputEditText clientInput;
    @BindView(R.id.phone_client_input)
    TextInputEditText phoneInput;
    @BindView(R.id.address_client_input)
    TextInputEditText addressInput;
    @BindView(R.id.description_input)
    TextInputEditText descriptionInput;
    @BindView(R.id.price_input)
    TextInputEditText priceInput;
    @BindView(R.id.start_date_input)
    TextInputEditText startDateInput;
    @BindView(R.id.end_date_input)
    TextInputEditText endDateInput;
    @BindView(R.id.bussines_number_input)
    TextInputEditText numberBussinesInput;
    @BindView(R.id.user_number_per_bussines_input)
    TextInputEditText userBussinesInput;
    @BindView(R.id.cashier_number_per_bussines_input)
    TextInputEditText cashierInput;
    @BindView(R.id.points_number_per_bussines_input)
    TextInputEditText pointsInput;
    @BindView(R.id.counter_number_per_bussines_input)
    TextInputEditText counter_number_per_bussines_input;
    @BindView(R.id.warehouse_number_per_bussines_input)
    TextInputEditText warehouse_number_per_bussines_input;
    @BindView(R.id.isInventory)
    CheckBox isInventory;
    @BindView(R.id.isMultipleCloseCash)
    CheckBox isMultipleCloseCash;
    @BindView(R.id.isControlInventory)
    CheckBox isControlInventory;
    @BindView(R.id.isCombinedPromotion)
    CheckBox isCombinedPromotion;
    @BindView(R.id.isDashboard)
    CheckBox isDashboard;
    @BindView(R.id.isReportExport)
    CheckBox isReportExport;
    @BindView(R.id.isModulePermissions)
    CheckBox isModulePermissions;
    @BindView(R.id.status_type)
    MaterialSpinner status_type;
    @BindView(R.id.group_show)
    Group group_show;

    private final String START = "START";
    private final String END = "END";
    private NewRequestViewModel mViewModel;
    private ProgressDialog mProgress;
    private DatePickerFragment mPicker;
    private String TYPE_DATE;
    private String DATE_FORMAT_START = "";
    private String DATE_FORMAT_END = "";
    private String statusPlan = "PENDIENTE";

    public static NewRequestFragment newInstance() {
        return new NewRequestFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                NavHostFragment.findNavController(NewRequestFragment.this).navigateUp();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.new_request_fragment, container, false);
        ButterKnife.bind(this, view);
        startDateInput.setInputType(InputType.TYPE_NULL);
        endDateInput.setInputType(InputType.TYPE_NULL);
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.plan_status, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status_type.setAdapter(adapter);
        status_type.setOnItemSelectedListener((view1, position, id, item) -> statusPlan = (String) item);
        status_type.setOnNothingSelectedListener(spinner -> statusPlan = "PENDIENTE");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelFactory viewModelFactory = Injected.viewModelFactory(getContext());
        mViewModel = new ViewModelProvider(this, viewModelFactory).get(NewRequestViewModel.class);
        mViewModel.getResult().observe(getViewLifecycleOwner(), requestPlanResult -> {
            mProgress.dismiss();
            mProgress.cancel();
            if (requestPlanResult == null) {
                return;
            }
            if (requestPlanResult.getSuccess() != null) {
                showSuccessResult(requestPlanResult.getSuccess());
            }
            if (requestPlanResult.getError() != null) {
                showErrorResult(requestPlanResult.getError());
            }
        });
        if (isUpdatedPlanRequest) {
            Log.d(getClass().getName(), reqPlanModel.toString());
            nameInput.setText(reqPlanModel.getName());
            clientInput.setText(reqPlanModel.getClient_name());
            clientInput.setEnabled(false);
            phoneInput.setText(reqPlanModel.getClient_phone());
            phoneInput.setEnabled(false);
            addressInput.setText(reqPlanModel.getClient_address());
            addressInput.setEnabled(false);
            descriptionInput.setText(reqPlanModel.getDescription());
            priceInput.setText(reqPlanModel.getPrice().replace(".00", ""));
            startDateInput.setText(reqPlanModel.getStart_date().substring(0, 10));
            endDateInput.setText(reqPlanModel.getEnd_date().substring(0, 10));
            DATE_FORMAT_START = reqPlanModel.getStart_date();
            DATE_FORMAT_END = reqPlanModel.getEnd_date();
            numberBussinesInput.setText(String.valueOf(reqPlanModel.getBussines_number()));
            userBussinesInput.setText(String.valueOf(reqPlanModel.getUser_number_per_pdv()));
            cashierInput.setText(String.valueOf(reqPlanModel.getCashier_number_per_pdv()));
            pointsInput.setText(String.valueOf(reqPlanModel.getPoints_of_sale_number()));
            counter_number_per_bussines_input.setText(String.valueOf(reqPlanModel.getCounter_per_pdv()));
            warehouse_number_per_bussines_input.setText(String.valueOf(reqPlanModel.getNumber_of_warehouse_per_pdv()));
            isInventory.setChecked(reqPlanModel.isSupplies_inventory());
            isControlInventory.setChecked(reqPlanModel.isInventory_control());
            isMultipleCloseCash.setChecked(reqPlanModel.isMultiple_closer_cashier());
            isCombinedPromotion.setChecked(reqPlanModel.isCombo_promotion_create());
            isDashboard.setChecked(reqPlanModel.isDashboard());
            isReportExport.setChecked(reqPlanModel.isExport_report());
            isModulePermissions.setChecked(reqPlanModel.isRole_andpermission_module());
            reqPlanModel.setRequest_response_by(CURRENT_SELLER.getSellerModel().getId());
            if (CURRENT_SELLER.getSellerModel().getType_seller().equals("MASTER CHIEF") || CURRENT_SELLER.getSellerModel().getType_seller().equals("SUPERVISOR")) {
                group_show.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showErrorResult(Integer error) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Error al solicitar")
                .setMessage(getString(error))
                .setPositiveButton("Aceptar", (dialog, which) -> dialog.dismiss()).create().show();
    }

    private void showSuccessResult(RequestReqPlanModel success) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Registro solicitado exitosamente")
                .setMessage("Se ha enviado la solicitud correctamente. Puede seguir las solicitudes en el apartado de solicitudes.")
                .setPositiveButton("Aceptar", (dialog, which) -> {
                    dialog.dismiss();
                    NavHostFragment.findNavController(this).navigateUp();
                }).create().show();
    }

    @OnClick(R.id.start_date_input)
    void setStartDateInput(View view) {
        TYPE_DATE = START;
        mPicker = new DatePickerFragment(this);
        mPicker.show(getParentFragmentManager(), "Fecha de inicio");
    }

    @OnClick(R.id.end_date_input)
    void setEndDateInput(View view) {
        TYPE_DATE = END;
        mPicker = new DatePickerFragment(this);
        mPicker.show(getParentFragmentManager(), "Fecha de finalización");
    }

    @OnClick(R.id.make_request)
    void makeRequest() {
        if (checkForm()) {
            mProgress = new ProgressDialog(getContext());
            mProgress.setMessage("Enviando solicitud...");
            mProgress.setCanceledOnTouchOutside(false);
            mProgress.show();
            RequestReqPlanModel pm;
            if (isUpdatedPlanRequest){
                pm = reqPlanModel;
            }else{
                pm = new RequestReqPlanModel();
            }
            pm.setName(nameInput.getText().toString());
            pm.setClient_name(clientInput.getText().toString());
            pm.setClient_phone(phoneInput.getText().toString());
            pm.setClient_address(addressInput.getText().toString());
            pm.setStatus_plan_request(statusPlan);
            pm.setDescription(descriptionInput.getText().toString());
            pm.setPrice(priceInput.getText().toString());
            pm.setStatus(true);
            pm.setStart_date(DATE_FORMAT_START);
            pm.setEnd_date(DATE_FORMAT_END);
            pm.setBussines_number(Integer.parseInt(numberBussinesInput.getText().toString()));
            pm.setUser_number_per_pdv(Integer.parseInt(userBussinesInput.getText().toString()));
            pm.setCashier_number_per_pdv(Integer.parseInt(cashierInput.getText().toString()));
            pm.setCreated_by(CURRENT_SELLER.getSellerModel().getId());
            pm.setPoints_of_sale_number(Integer.parseInt(pointsInput.getText().toString()));
            pm.setCreated_at(NOW());
            pm.setInventory_control(isInventory.isChecked());
            pm.setMultiple_closer_cashier(isMultipleCloseCash.isChecked());
            pm.setInventory_control(isControlInventory.isChecked());
            pm.setCombo_promotion_create(isCombinedPromotion.isChecked());
            pm.setDashboard(isDashboard.isChecked());
            pm.setExport_report(isReportExport.isChecked());
            pm.setRole_andpermission_module(isModulePermissions.isChecked());
            if (isUpdatedPlanRequest) {
                mViewModel.updateSendRequest(pm);
            } else {
                mViewModel.sendRequest(pm);
            }
            isUpdatedPlanRequest = false;
        }
    }

    @SuppressWarnings("ConstantConditions")
    private boolean checkForm() {
        boolean[] checker = new boolean[12];
        if (nameInput.getText().toString().isEmpty()) {
            nameLayout.setError("Este campo no puede estar vacio");
            checker[0] = false;
        } else {
            checker[0] = true;
            nameLayout.setError(null);
        }

        if (clientInput.getText().toString().isEmpty()) {
            clientLayout.setError("Este campo no puede estar vacio");
            checker[1] = false;
        } else {
            checker[1] = true;
            clientLayout.setError(null);
        }

        if (phoneInput.getText().toString().isEmpty()) {
            phoneLayout.setError("Este campo no puede estar vacio");
            checker[2] = false;
        } else {
            checker[2] = true;
            phoneLayout.setError(null);
        }

        if (addressInput.getText().toString().isEmpty()) {
            addressLayout.setError("Este campo no puede estar vacio");
            checker[3] = false;
        } else {
            checker[3] = true;
            addressLayout.setError(null);
        }

        if (descriptionInput.getText().toString().isEmpty()) {
            descriptionLayout.setError("Este campo no puede estar vacio");
            checker[4] = false;
        } else {
            checker[4] = true;
            descriptionLayout.setError(null);
        }

        if (priceInput.getText().toString().isEmpty()) {
            priceLayout.setError("Este campo no puede estar vacio");
            checker[5] = false;
        } else {
            checker[5] = true;
            priceLayout.setError(null);
        }

        if (counter_number_per_bussines_input.getText().toString().isEmpty()) {
            counter_number_per_bussines_layout.setError("Este campo no puede estar vacio");
            checker[6] = false;
        } else {
            checker[6] = true;
            counter_number_per_bussines_layout.setError(null);
        }

        if (warehouse_number_per_bussines_input.getText().toString().isEmpty()) {
            warehouse_number_per_bussines_layout.setError("Este campo no puede estar vacio");
            checker[7] = false;
        } else {
            checker[7] = true;
            warehouse_number_per_bussines_layout.setError(null);
        }

        if (numberBussinesInput.getText().toString().isEmpty()) {
            numberBussinesLayout.setError("Este campo no puede estar vacio");
            checker[8] = false;
        } else {
            checker[8] = true;
            numberBussinesLayout.setError(null);
        }

        if (userBussinesInput.getText().toString().isEmpty()) {
            userBussinesLayout.setError("Este campo no puede estar vacio");
            checker[9] = false;
        } else {
            checker[9] = true;
            userBussinesLayout.setError(null);
        }

        if (cashierInput.getText().toString().isEmpty()) {
            cashierLayout.setError("Este campo no puede estar vacio");
            checker[10] = false;
        } else {
            checker[10] = true;
            cashierLayout.setError(null);
        }

        if (pointsInput.getText().toString().isEmpty()) {
            pointsLayout.setError("Este campo no puede estar vacio");
            checker[11] = false;
        } else {
            checker[11] = true;
            pointsLayout.setError(null);
        }
        return checker[0] && checker[1] && checker[2] && checker[3] && checker[4] && checker[5] && checker[6] && checker[7] && checker[8] && checker[9] && checker[10] && checker[11];
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS-05", Locale.US);
        SimpleDateFormat __format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String date_selected;
        String formatter_date;
        if (monthOfYear <= 9)
            date_selected = String.format("%s-0%s-%s %s", year, monthOfYear + 1, dayOfMonth, TIME_NOW());
        else
            date_selected = String.format("%s-%s-%s %s", year, monthOfYear + 1, dayOfMonth, TIME_NOW());
        try {
            Date last_date = __format.parse(date_selected);
            formatter_date = format.format(last_date);
        } catch (Exception e) {
            formatter_date = date_selected;
        }
        if (TYPE_DATE.equals(START)) {
            startDateInput.setText(date_selected);
            DATE_FORMAT_START = formatter_date;
        }
        if (TYPE_DATE.equals(END)) {
            endDateInput.setText(date_selected);
            DATE_FORMAT_END = formatter_date;
        }
    }
}
