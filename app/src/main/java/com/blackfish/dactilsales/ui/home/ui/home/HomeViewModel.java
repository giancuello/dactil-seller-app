package com.blackfish.dactilsales.ui.home.ui.home;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponsePlanModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;
import com.blackfish.dactilsales.data.repository.HomeRepository;
import com.blackfish.dactilsales.viewholders.PlansAdapter;

import java.util.List;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<PlansAdapter> mAdapter;
    private MutableLiveData<Integer> mEmptySource;
    private HomeRepository homeRepository;
    private HomeFragment homeFragment;
    @NonNull
    private Context context;

    public HomeViewModel(@NonNull Context context, HomeRepository homeRepository) {
        mAdapter = new MutableLiveData<>();
        mEmptySource = new MutableLiveData<>();
        this.context = context;
        this.homeRepository = homeRepository;
    }

    private void setPlanList(List<PlanModel> requestPlanModelList) {
        PlansAdapter adapter = new PlansAdapter(requestPlanModelList, context);
        adapter.setFragment(homeFragment);
        mAdapter.setValue(adapter);
    }

    public LiveData<PlansAdapter> getAdapter() {
        return mAdapter;
    }

    public LiveData<Integer> getEmptySource(){
        return mEmptySource;
    }

    public void setPlanList() {
        Result<ResponseRequestModelList> modelList = homeRepository.getPlanListModel();
        if (modelList instanceof Result.Success) {
            ResponsePlanModelList data = ((Result.Success<ResponsePlanModelList>) modelList).getData();
            if (data.getCode() == 1 && data.getRecordsFiltered() > 0) {
                setPlanList(data.getData());
            } else {
                mEmptySource.setValue(R.string.empty_source_string);
            }
        }
    }

    public void setFragment(HomeFragment homeFragment) {
        this.homeFragment = homeFragment;
    }
}