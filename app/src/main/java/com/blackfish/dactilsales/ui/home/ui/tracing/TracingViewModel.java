package com.blackfish.dactilsales.ui.home.ui.tracing;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.consumer.ServiceGenerator;
import com.blackfish.dactilsales.data.interfaces.OwnerClient;
import com.blackfish.dactilsales.data.interfaces.PlansClient;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseOwnerModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModel;
import com.blackfish.dactilsales.util.Common;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;

public class TracingViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private ExecutorService executor
            = Executors.newSingleThreadExecutor();
    @NonNull
    private Context context;

    public TracingViewModel(@NonNull Context context) {
        mText = new MutableLiveData<>();
        mText.setValue("Sin seguimientos.");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public ResponseOwnerModel getOwners() {
        try {
            Future<ResponseOwnerModel> requestModelFuture = getOwnersFuture();
            while (!requestModelFuture.isDone()) {
                System.out.println("Requesting...");

                Thread.sleep(300);
            }
            ResponseOwnerModel rrm = requestModelFuture.get();
            return rrm;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseOwnerModel();
        }
    }

    public ResponseOwnerModel searchFilter(String filter) {
        try {
            Future<ResponseOwnerModel> requestModelFuture = searchFuture(filter);
            while (!requestModelFuture.isDone()) {
                System.out.println("Requesting...");

                Thread.sleep(300);
            }
            ResponseOwnerModel rrm = requestModelFuture.get();
            return rrm;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseOwnerModel();
        }
    }

    private Future<ResponseOwnerModel> getOwnersFuture() {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        OwnerClient client = ServiceGenerator.createService(OwnerClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseOwnerModel> call = client.getOwner();
            return call.execute().body();
        });
    }

    private Future<ResponseOwnerModel> searchFuture(String filter) {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        OwnerClient client = ServiceGenerator.createService(OwnerClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseOwnerModel> call = client.searchOwner(filter);
            return call.execute().body();
        });
    }
}