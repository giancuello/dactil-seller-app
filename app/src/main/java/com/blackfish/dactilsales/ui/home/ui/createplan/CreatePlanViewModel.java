package com.blackfish.dactilsales.ui.home.ui.createplan;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.data.model.requestmodel.RequestPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModel;
import com.blackfish.dactilsales.data.repository.RequestRepository;

public class CreatePlanViewModel extends ViewModel {
    private MutableLiveData<String> mDataResult;
    @NonNull
    private Context context;
    private RequestRepository requestRepository;

    public CreatePlanViewModel(@NonNull Context context, RequestRepository requestRepository) {
        this.context = context;
        this.requestRepository = requestRepository;
        mDataResult = new MutableLiveData<>();
    }

    public LiveData<String> getDataResult() {
        return mDataResult;
    }

    public void createPlan(PlanModel planModel) {
        Result<RequestPlanModel> rplanModel = requestRepository.createPlan(planModel);
        if (rplanModel instanceof Result.Success) {
            RequestPlanModel data = ((Result.Success<RequestPlanModel>) rplanModel).getData();
            mDataResult.setValue(data.getMessage());
        } else {
            mDataResult.setValue(context.getString(R.string.error_create_plan));
        }
    }
}
