package com.blackfish.dactilsales.ui.home.ui.requests;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.ui.home.ui.newrequest.NewRequestFragment;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.blackfish.dactilsales.viewholders.RequestPlanAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestsFragment extends Fragment {

    @BindView(R.id.swipe_refresh_request)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.plans_recycler_request)
    RecyclerView plansRecycler;
    @BindView(R.id.empty_source_request)
    TextView emptySource;

    private RequestsViewModel requestsViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                NavHostFragment.findNavController(RequestsFragment.this).navigateUp();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View root = inflater.inflate(R.layout.fragment_requests, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelFactory viewModelFactory = Injected.viewModelFactory(getContext());
        requestsViewModel =
                new ViewModelProvider(this, viewModelFactory).get(RequestsViewModel.class);
        requestsViewModel.getText().observe(getViewLifecycleOwner(), integer -> {
            Log.d(getClass().getName(), "Value from integer");
            swipeRefresh.setRefreshing(false);
            swipeRefresh.setVisibility(View.GONE);
            emptySource.setVisibility(View.VISIBLE);
            emptySource.setText(getString(integer));
        });

        requestsViewModel.getAdapter().observe(getViewLifecycleOwner(), new Observer<RequestPlanAdapter>() {
            @Override
            public void onChanged(RequestPlanAdapter requestPlanAdapter) {
                Log.d(getClass().getName(), "Value from adapter");
                swipeRefresh.setVisibility(View.VISIBLE);
                emptySource.setVisibility(View.GONE);
                requestPlanAdapter.setRequestsFragment(RequestsFragment.this);
                plansRecycler.setAdapter(requestPlanAdapter);
                requestPlanAdapter.notifyDataSetChanged();
                swipeRefresh.setRefreshing(false);
            }
        });
        plansRecycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        plansRecycler.setLayoutManager(layoutManager);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(this::getList);
        swipeRefresh.post(this::getList);
    }

    private void getList() {
        swipeRefresh.setRefreshing(true);
        requestsViewModel.getList();
    }
}