package com.blackfish.dactilsales.ui.home.ui.owner;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.model.CityModel;
import com.blackfish.dactilsales.data.model.CountryModel;
import com.blackfish.dactilsales.data.model.DepartmentModel;
import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.data.model.TypeDocumentModel;
import com.blackfish.dactilsales.data.model.ValidUser;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCityModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCountryModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCreateOwner;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseDepartmentModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseOwnerModel;
import com.blackfish.dactilsales.data.repository.OwnerRepository;

import java.util.ArrayList;
import java.util.List;

public class OwnerViewModel extends ViewModel {

    private OwnerRepository ownerRepository;
    @NonNull
    private Context context;
    private MutableLiveData<List<CityModel>> mCityModel;
    private MutableLiveData<List<CountryModel>> mCountryModel;
    private MutableLiveData<List<DepartmentModel>> mDepartmentModel;
    private MutableLiveData<List<TypeDocumentModel>> mTypeDocumentModel;
    private MutableLiveData<String> mResultSearch;
    private MutableLiveData<String> mResultSave;

    public OwnerViewModel(@NonNull Context context, OwnerRepository ownerRepository) {
        this.context = context;
        this.ownerRepository = ownerRepository;
        this.mCityModel = new MutableLiveData<>();
        this.mCountryModel = new MutableLiveData<>();
        this.mDepartmentModel = new MutableLiveData<>();
        this.mTypeDocumentModel = new MutableLiveData<>();
        this.mResultSearch = new MutableLiveData<>();
        this.mResultSave = new MutableLiveData<>();
    }

    public LiveData<List<CityModel>> getObserverCityModel() {
        return mCityModel;
    }

    public LiveData<List<CountryModel>> getObserverCountryModel() {
        return mCountryModel;
    }

    public LiveData<List<DepartmentModel>> getObserverDepartmentModel() {
        return mDepartmentModel;
    }

    public LiveData<List<TypeDocumentModel>> getObserverTypeDocumentModel() {
        return mTypeDocumentModel;
    }

    public LiveData<String> getRsult() {
        return mResultSearch;
    }

    public LiveData<String> getResultSave(){
        return mResultSave;
    }

    public void createOwner(OwnerModel ownerModel){
        Result<ResponseCreateOwner> ownerModelResult = ownerRepository.createOwner(ownerModel);
        if (ownerModelResult instanceof Result.Success){
            ResponseCreateOwner data = ((Result.Success<ResponseCreateOwner>) ownerModelResult).getData();
            if (data.getCode() == 1){
                mResultSave.setValue(data.getMessage());
            }else{
                mResultSave.setValue("Error al intentar registrar el Propietario.");
            }
        }
    }

    public void getCityList() {
        Result<ResponseCityModelList> cityModelListResult = ownerRepository.getCityList();
        if (cityModelListResult instanceof Result.Success) {
            ResponseCityModelList data = ((Result.Success<ResponseCityModelList>) cityModelListResult).getData();
            if (data.getData() != null && !data.getData().isEmpty()) {
                mCityModel.setValue(data.getData());
            } else {
                mCityModel.setValue(new ArrayList<>());
            }
        }
    }

    public void getCountryList() {
        Result<ResponseCountryModelList> cityModelListResult = ownerRepository.getCountryList();
        if (cityModelListResult instanceof Result.Success) {
            ResponseCountryModelList data = ((Result.Success<ResponseCountryModelList>) cityModelListResult).getData();
            if (data.getData() != null && !data.getData().isEmpty()) {
                mCountryModel.setValue(data.getData());
            } else {
                mCountryModel.setValue(new ArrayList<>());
            }
        }
    }

    public void getDepartmentList() {
        Result<ResponseDepartmentModelList> cityModelListResult = ownerRepository.getDepartmentList();
        if (cityModelListResult instanceof Result.Success) {
            ResponseDepartmentModelList data = ((Result.Success<ResponseDepartmentModelList>) cityModelListResult).getData();
            if (data.getData() != null && !data.getData().isEmpty()) {
                mDepartmentModel.setValue(data.getData());
            } else {
                mDepartmentModel.setValue(new ArrayList<>());
            }
        }
    }

    public void getTypeDocumentList() {
        Result<List<TypeDocumentModel>> cityModelListResult = ownerRepository.getTypeDocumentList();
        if (cityModelListResult instanceof Result.Success) {
            List<TypeDocumentModel> data = ((Result.Success<List<TypeDocumentModel>>) cityModelListResult).getData();
            if (data != null && !data.isEmpty()) {
                mTypeDocumentModel.setValue(data);
            } else {
                mTypeDocumentModel.setValue(new ArrayList<>());
            }
        }
    }

    public void searchIfExist(String search) {
        Result<ValidUser> valid = ownerRepository.validUsername(search);
        if (valid instanceof Result.Success) {
            ValidUser data = ((Result.Success<ValidUser>) valid).getData();
            mResultSearch.setValue((!data.data) ? "1" : data.message);
        }
    }
}
