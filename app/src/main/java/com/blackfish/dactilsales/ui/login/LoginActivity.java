package com.blackfish.dactilsales.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.blackfish.dactilsales.R;
import com.blackfish.dactilsales.ui.home.DashboardActivity;
import com.blackfish.dactilsales.util.Injected;
import com.blackfish.dactilsales.util.ViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;

import static com.blackfish.dactilsales.util.Common.KEY_REMEMBER;
import static com.blackfish.dactilsales.util.Common.hideKeyboard;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.username)
    TextInputEditText usernameEditText;
    @BindView(R.id.password)
    TextInputEditText passwordEditText;
    @BindView(R.id.username_layout)
    TextInputLayout usernameTextLayout;
    @BindView(R.id.password_layout)
    TextInputLayout passwordTextLayout;
    @BindView(R.id.login)
    Button loginButton;
    @BindView(R.id.loading)
    ProgressBar loadingProgressBar;
    @BindView(R.id.remember_check)
    CheckBox rememberCheck;
    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_login);
        initView();

    }

    private void initView() {
        ButterKnife.bind(this);
        Paper.init(this);

        ViewModelFactory viewModelFactory = Injected.viewModelFactory(this);
        loginViewModel = new ViewModelProvider(this, viewModelFactory).get(LoginViewModel.class);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    Log.d(getClass().getName(), getString(loginFormState.getUsernameError()));
                    usernameTextLayout.setError(getString(loginFormState.getUsernameError()));
                } else {
                    usernameTextLayout.setError(null);
                }
                if (loginFormState.getPasswordError() != null) {
                    Log.d(getClass().getName(), getString(loginFormState.getPasswordError()));
                    passwordTextLayout.setError(getString(loginFormState.getPasswordError()));
                } else {
                    passwordTextLayout.setError(null);
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, loginResult -> {
            if (loginResult == null) {
                return;
            }
            if (loginResult.getError() != null) {
                showLoginFailed(loginResult.getError());
            }
            if (loginResult.getSuccess() != null) {
                updateUiWithUser(loginResult.getSuccess());
            }
            setResult(Activity.RESULT_OK);

            //Complete and destroy login activity once successful
            //finish();
        });

        loginButton.setOnClickListener(v -> {
            loadingProgressBar.setVisibility(View.VISIBLE);
            if (usernameEditText.getText().toString().isEmpty())
                usernameTextLayout.setError("Este campo no puede estar vació.");
            else
                usernameTextLayout.setError(null);
            if (passwordEditText.getText().toString().isEmpty())
                passwordTextLayout.setError("Este campo no debe estar vació.");
            else
                passwordTextLayout.setError(null);

            if (!usernameEditText.getText().toString().isEmpty() && !passwordEditText.getText().toString().isEmpty() && passwordEditText.getText().toString().length() > 6) {
                hideKeyboard(LoginActivity.this);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });

        rememberCheck.setOnCheckedChangeListener((buttonView, isChecked) -> Paper.book().write(KEY_REMEMBER, isChecked));

        checkIfLoginIsRemembered();
    }

    private void checkIfLoginIsRemembered() {
        boolean isRemember;
        try{
            isRemember = Paper.book().read(KEY_REMEMBER);
        }catch (Exception e){
            isRemember = false;
        }
        if (isRemember){
            loginViewModel.isLoginRemembered();
        }
    }

    private void updateUiWithUser(LoggedInUserView model) {
        loadingProgressBar.setVisibility(View.GONE);
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        loadingProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }
}
