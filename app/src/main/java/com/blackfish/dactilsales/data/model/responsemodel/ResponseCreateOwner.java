package com.blackfish.dactilsales.data.model.responsemodel;

import com.blackfish.dactilsales.data.model.OwnerModel;

public class ResponseCreateOwner {
    private String message;
    private int code;
    private OwnerModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public OwnerModel getData() {
        return data;
    }

    public void setData(OwnerModel data) {
        this.data = data;
    }
}
