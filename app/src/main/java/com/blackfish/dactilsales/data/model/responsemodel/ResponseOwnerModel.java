package com.blackfish.dactilsales.data.model.responsemodel;

import com.blackfish.dactilsales.data.model.OwnerModel;

import java.util.List;

public class ResponseOwnerModel {
    private String message;
    private int code;
    private List<OwnerModel> data;
    private int draw;
    private int recordsTotal;
    private int recordsFiltered;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<OwnerModel> getData() {
        return data;
    }

    public void setData(List<OwnerModel> data) {
        this.data = data;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }
}
