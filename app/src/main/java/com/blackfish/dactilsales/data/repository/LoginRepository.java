package com.blackfish.dactilsales.data.repository;

import android.util.Log;

import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.datasource.LoginDataSource;
import com.blackfish.dactilsales.data.model.SellerToken;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private LoginDataSource dataSource;

    private SellerToken user = null;

    // private constructor : singleton access
    private LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        user = null;
        dataSource.logout();
    }

    private void setLoggedInUser(SellerToken user) {
        this.user = user;
    }

    public Result<SellerToken> login(String username, String password) {
        // handle login
        Result<SellerToken> result = dataSource.login(username, password);
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<SellerToken>) result).getData());
        }
        return result;
    }
}
