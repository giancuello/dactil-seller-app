package com.blackfish.dactilsales.data.model.requestmodel;

import com.blackfish.dactilsales.data.model.PlanModel;

public class RequestPlanModel {
    private String message;
    private int code;
    private PlanModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public PlanModel getData() {
        return data;
    }

    public void setData(PlanModel data) {
        this.data = data;
    }
}
