package com.blackfish.dactilsales.data.datasource;

import android.util.Log;

import com.blackfish.dactilsales.consumer.ServiceGenerator;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.interfaces.PlansClient;
import com.blackfish.dactilsales.data.model.responsemodel.ResponsePlanModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;
import com.blackfish.dactilsales.util.Common;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;

public class HomeDataSource {
    private ExecutorService executor
            = Executors.newSingleThreadExecutor();

    public Result<ResponseRequestModelList> getModelList(){
        try{
            Future<ResponsePlanModelList> requestModelFuture = getModelListAuth();
            while (!requestModelFuture.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            ResponsePlanModelList rrm = requestModelFuture.get();
            return new Result.Success<>(rrm);
        }catch (Exception e){
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error to request Response to Plan List", e));
        }
    }

    private Future<ResponsePlanModelList> getModelListAuth() {
        String token = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        PlansClient plansClient = ServiceGenerator.createService(PlansClient.class, token);
        return executor.submit(() -> {
            Call<ResponsePlanModelList> call = plansClient.getPlans();
            return call.execute().body();
        });
    }
}
