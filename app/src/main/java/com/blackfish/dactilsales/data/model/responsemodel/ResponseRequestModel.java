package com.blackfish.dactilsales.data.model.responsemodel;

import com.blackfish.dactilsales.data.model.RequestReqPlanModel;

public class ResponseRequestModel {
    private String message;
    private int code;
    private RequestReqPlanModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public RequestReqPlanModel getData() {
        return data;
    }

    public void setData(RequestReqPlanModel data) {
        this.data = data;
    }

    public ResponseRequestModel(String message, int code, RequestReqPlanModel data) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    public ResponseRequestModel() {
    }

    @Override
    public String toString() {
        return "ResponseRequestModel{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", data=" + data +
                '}';
    }
}
