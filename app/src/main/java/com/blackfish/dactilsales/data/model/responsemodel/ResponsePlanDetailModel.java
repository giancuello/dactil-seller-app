package com.blackfish.dactilsales.data.model.responsemodel;

import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;

import java.util.List;

public class ResponsePlanDetailModel {
    private int id;
    private int type_document;
    private String document;
    private String name;
    private String first_surname;
    private String second_surname;
    private String address;
    private int city;
    private String username;
    private int country;
    private int departament;
    private String phone1;
    private String phone2;
    private String email;
    private String created_at;
    private String updated_at;
    private boolean status;
    private List<PlanModel> plan;
    private List<RequestReqPlanModel> onwer_request;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType_document() {
        return type_document;
    }

    public void setType_document(int type_document) {
        this.type_document = type_document;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_surname() {
        return first_surname;
    }

    public void setFirst_surname(String first_surname) {
        this.first_surname = first_surname;
    }

    public String getSecond_surname() {
        return second_surname;
    }

    public void setSecond_surname(String second_surname) {
        this.second_surname = second_surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public int getDepartament() {
        return departament;
    }

    public void setDepartament(int departament) {
        this.departament = departament;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<PlanModel> getPlan() {
        return plan;
    }

    public void setPlan(List<PlanModel> plan) {
        this.plan = plan;
    }

    public List<RequestReqPlanModel> getOnwer_request() {
        return onwer_request;
    }

    public void setOnwer_request(List<RequestReqPlanModel> onwer_request) {
        this.onwer_request = onwer_request;
    }
}
