package com.blackfish.dactilsales.data.datasource;

import android.util.Log;

import com.blackfish.dactilsales.consumer.ServiceGenerator;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.interfaces.DataClient;
import com.blackfish.dactilsales.data.interfaces.OwnerClient;
import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.data.model.TypeDocumentModel;
import com.blackfish.dactilsales.data.model.ValidUser;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCityModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCountryModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCreateOwner;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseDepartmentModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseOwnerModel;
import com.blackfish.dactilsales.util.Common;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;

public class OwnerDataSource {
    
    private ExecutorService executor
            = Executors.newSingleThreadExecutor();

    @SuppressWarnings("unchecked")
    public Result<ResponseCreateOwner> createOwner(OwnerModel ownerModel) {
        try {
            Future<ResponseCreateOwner> requestModelFuture = makeNetworkRequestOwner(ownerModel);
            while (!requestModelFuture.isDone()) {
                System.out.println("Requesting...");
                Thread.sleep(200);
            }
            ResponseCreateOwner rrm = requestModelFuture.get();
            return new Result.Success<>(rrm);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    @SuppressWarnings("unchecked")
    public Result<ResponseCityModelList> getCityList() {
        try {
            Future<ResponseCityModelList> response = makeRequestCityList();
            while (!response.isDone()) {
                System.out.println("Requesting...");
                Thread.sleep(200);
            }
            ResponseCityModelList cml = response.get();
            return new Result.Success<>(cml);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error trying to download the list", e));
        }
    }

    @SuppressWarnings("unchecked")
    public Result<ResponseCountryModelList> getCountryList() {
        try {
            Future<ResponseCountryModelList> response = makeRequestCountryList();
            while (!response.isDone()) {
                System.out.println("Requesting...");
                Thread.sleep(200);
            }
            ResponseCountryModelList cml = response.get();
            return new Result.Success<>(cml);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error trying to download the list", e));
        }
    }

    @SuppressWarnings("unchecked")
    public Result<ResponseDepartmentModelList> getDepartmentList() {
        try {
            Future<ResponseDepartmentModelList> response = makeRequestDepartmentList();
            while (!response.isDone()) {
                System.out.println("Requesting...");
                Thread.sleep(200);
            }
            ResponseDepartmentModelList dml = response.get();
            return new Result.Success<>(dml);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error trying to download the list", e));
        }
    }

    @SuppressWarnings("unchecked")
    public Result<List<TypeDocumentModel>> getTypeDocumentList() {
        try {
            Future<List<TypeDocumentModel>> response = makeRequestTypeDocumentList();
            while (!response.isDone()) {
                System.out.println("Requesting...");
                Thread.sleep(200);
            }
            List<TypeDocumentModel> dml = response.get();
            return new Result.Success<>(dml);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error trying to download the list", e));
        }
    }

    @SuppressWarnings("unchecked")
    public Result<ValidUser> validUsername(String search) {
        try {
            Future<ValidUser> response = makeRequestValidUsername(search);
            while (!response.isDone()) {
                System.out.println("Requesting...");
                Thread.sleep(200);
            }
            ValidUser dml = response.get();
            return new Result.Success<>(dml);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error trying to download the user", e));
        }
    }

    private Future<ValidUser> makeRequestValidUsername(String search) {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        DataClient client = ServiceGenerator.createService(DataClient.class, makeToken);
        return executor.submit(() -> {
            Call<ValidUser> call = client.validateUser(search);
            return call.execute().body();
        });
    }

    private Future<ResponseCreateOwner> makeNetworkRequestOwner(OwnerModel ownerModel) {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        OwnerClient client = ServiceGenerator.createService(OwnerClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseCreateOwner> call = client.createOwner(ownerModel);
            return call.execute().body();
        });
    }

    private Future<ResponseCityModelList> makeRequestCityList() {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        DataClient client = ServiceGenerator.createService(DataClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseCityModelList> call = client.getCities();
            return call.execute().body();
        });
    }

    private Future<ResponseCountryModelList> makeRequestCountryList() {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        DataClient client = ServiceGenerator.createService(DataClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseCountryModelList> call = client.getCountry();
            return call.execute().body();
        });
    }

    private Future<ResponseDepartmentModelList> makeRequestDepartmentList() {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        DataClient client = ServiceGenerator.createService(DataClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseDepartmentModelList> call = client.getDepartment();
            return call.execute().body();
        });
    }

    private Future<List<TypeDocumentModel>> makeRequestTypeDocumentList() {
        String makeToken = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        DataClient client = ServiceGenerator.createService(DataClient.class, makeToken);
        return executor.submit(() -> {
            Call<List<TypeDocumentModel>> call = client.getTypeDocument();
            return call.execute().body();
        });
    }

}
