package com.blackfish.dactilsales.data.repository;

import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.datasource.HomeDataSource;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;

public class HomeRepository {
    private static volatile HomeRepository instance;
    private HomeDataSource homeDataSource;

    private HomeRepository(HomeDataSource homeDataSource) {
        this.homeDataSource = homeDataSource;
    }

    public static HomeRepository getInstance(HomeDataSource homeDataSource){
        if (instance == null){
            instance = new HomeRepository(homeDataSource);
        }
        return instance;
    }

    public Result<ResponseRequestModelList> getPlanListModel(){
        return homeDataSource.getModelList();
    }
}
