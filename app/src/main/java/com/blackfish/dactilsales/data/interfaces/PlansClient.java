package com.blackfish.dactilsales.data.interfaces;

import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.requestmodel.RequestPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponsePlanModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PlansClient {
    @POST("comercial-plan-request-list-create")
    Call<ResponseRequestModel> sendRequestPlan(@Body RequestReqPlanModel requestReqPlanModel);

    @PUT("comercial-plan-request-detail/{id}")
    Call<ResponseRequestModel> updateRequestPlan(@Body RequestReqPlanModel requestReqPlanModel, @Path("id") int id);

    @GET("comercial-plan-request-list-create")
    Call<ResponseRequestModelList> getRequestPlansByFilter(@Query("search") String filter);

    @GET("comercial-plan-request-list-create")
    Call<ResponseRequestModelList> getRequestPlans();

    @GET("comercial-plan-list-create")
    Call<ResponsePlanModelList> getPlans();

    @POST("comercial-plan-list-create")
    Call<RequestPlanModel> createPlan(@Body PlanModel planModel);
}
