package com.blackfish.dactilsales.data.interfaces;

import com.blackfish.dactilsales.data.model.TypeDocumentModel;
import com.blackfish.dactilsales.data.model.ValidUser;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCityModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCountryModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseDepartmentModelList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DataClient {

    @GET("cities")
    Call<ResponseCityModelList> getCities();

    @GET("country")
    Call<ResponseCountryModelList> getCountry();

    @GET("department")
    Call<ResponseDepartmentModelList> getDepartment();

    @GET("type_document")
    Call<List<TypeDocumentModel>> getTypeDocument();

    @GET("valid-username/{search}")
    Call<ValidUser> validateUser(@Path("search") String search);
}
