package com.blackfish.dactilsales.data.datasource;

import android.util.Log;

import com.blackfish.dactilsales.consumer.ServiceGenerator;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.interfaces.UserClient;
import com.blackfish.dactilsales.data.model.LoggedInUser;
import com.blackfish.dactilsales.data.model.SellerToken;
import com.blackfish.dactilsales.data.model.requestmodel.RequestSellerModel;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    private ExecutorService executor
            = Executors.newSingleThreadExecutor();

    public Result<SellerToken> login(String username, String password) {
        try {
            Future<SellerToken> sellerToken = sendNetworkRequest(username, password);
            while (!sellerToken.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            SellerToken st = sellerToken.get();
            Future<RequestSellerModel> sellerModelFuture = retrieveInformationUserRequest(st.getData());
            while (!sellerModelFuture.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            if (sellerModelFuture.get().getCode() == 1){
                st.setSellerModel(sellerModelFuture.get().getData());
            }else{
                return new Result.Error(new IOException("Error on retrieve user info"));
            }
            return new Result.Success<>(st);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    private Future<SellerToken> sendNetworkRequest(String username, String password) {
        UserClient client = ServiceGenerator.createService(UserClient.class);
        LoggedInUser currentUser = new LoggedInUser(
                username,
                password);
        return executor.submit(() -> {
            Call<SellerToken> call = client.loginUser(currentUser);
            SellerToken sellerToken = call.execute().body();
            sellerToken.setUser(currentUser);
            return sellerToken;
        });
    }

    private Future<RequestSellerModel> retrieveInformationUserRequest(String token){
        String makeToken = String.format("Bearer %s", token);
        UserClient client = ServiceGenerator.createService(UserClient.class, makeToken);
        return executor.submit(() -> {
           Call<RequestSellerModel> call = client.retrieveSellerInformation();
           return call.execute().body();
        });
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
