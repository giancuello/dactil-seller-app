package com.blackfish.dactilsales.data.model;

public class RequestReqPlanModel {

    private int id;
    private String name;
    private String client_name;
    private String client_phone;
    private String client_address;
    private String status_plan_request;
    private String type_plan;
    private String description;
    private String price;
    private boolean status;
    private String start_date;
    private String end_date;
    private int bussines_number;
    private int user_number_per_pdv;
    private int cashier_number_per_pdv;
    private boolean supplies_inventory;
    private boolean multiple_closer_cashier;
    private int counter_per_pdv;
    private int number_of_warehouse_per_pdv;
    private boolean inventory_control;
    private boolean combo_promotion_create;
    private boolean dashboard;
    private boolean export_report;
    private boolean role_andpermission_module;
    private int points_of_sale_number;
    private int created_by;
    private int request_response_by;
    private String created_at;

    public RequestReqPlanModel(int id, String name, String client_name, String client_phone, String client_address, String status_plan_request, String type_plan, String description, String price, boolean status, String start_date, String end_date, int bussines_number, int user_number_per_pdv, int cashier_number_per_pdv, boolean supplies_inventory, boolean multiple_closer_cashier, int counter_per_pdv, int number_of_warehouse_per_pdv, boolean inventory_control, boolean combo_promotion_create, boolean dashboard, boolean export_report, boolean role_andpermission_module, int points_of_sale_number, int created_by, int request_response_by, String created_at) {
        this.id = id;
        this.name = name;
        this.client_name = client_name;
        this.client_phone = client_phone;
        this.client_address = client_address;
        this.status_plan_request = status_plan_request;
        this.type_plan = type_plan;
        this.description = description;
        this.price = price;
        this.status = status;
        this.start_date = start_date;
        this.end_date = end_date;
        this.bussines_number = bussines_number;
        this.user_number_per_pdv = user_number_per_pdv;
        this.cashier_number_per_pdv = cashier_number_per_pdv;
        this.supplies_inventory = supplies_inventory;
        this.multiple_closer_cashier = multiple_closer_cashier;
        this.counter_per_pdv = counter_per_pdv;
        this.number_of_warehouse_per_pdv = number_of_warehouse_per_pdv;
        this.inventory_control = inventory_control;
        this.combo_promotion_create = combo_promotion_create;
        this.dashboard = dashboard;
        this.export_report = export_report;
        this.role_andpermission_module = role_andpermission_module;
        this.points_of_sale_number = points_of_sale_number;
        this.created_by = created_by;
        this.request_response_by = request_response_by;
        this.created_at = created_at;
    }

    public RequestReqPlanModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_phone() {
        return client_phone;
    }

    public void setClient_phone(String client_phone) {
        this.client_phone = client_phone;
    }

    public String getClient_address() {
        return client_address;
    }

    public void setClient_address(String client_address) {
        this.client_address = client_address;
    }

    public String getStatus_plan_request() {
        return status_plan_request;
    }

    public void setStatus_plan_request(String status_plan_request) {
        this.status_plan_request = status_plan_request;
    }

    public String getType_plan() {
        return type_plan;
    }

    public void setType_plan(String type_plan) {
        this.type_plan = type_plan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getBussines_number() {
        return bussines_number;
    }

    public void setBussines_number(int bussines_number) {
        this.bussines_number = bussines_number;
    }

    public int getUser_number_per_pdv() {
        return user_number_per_pdv;
    }

    public void setUser_number_per_pdv(int user_number_per_pdv) {
        this.user_number_per_pdv = user_number_per_pdv;
    }

    public int getCashier_number_per_pdv() {
        return cashier_number_per_pdv;
    }

    public void setCashier_number_per_pdv(int cashier_number_per_pdv) {
        this.cashier_number_per_pdv = cashier_number_per_pdv;
    }

    public boolean isSupplies_inventory() {
        return supplies_inventory;
    }

    public void setSupplies_inventory(boolean supplies_inventory) {
        this.supplies_inventory = supplies_inventory;
    }

    public boolean isMultiple_closer_cashier() {
        return multiple_closer_cashier;
    }

    public void setMultiple_closer_cashier(boolean multiple_closer_cashier) {
        this.multiple_closer_cashier = multiple_closer_cashier;
    }

    public int getCounter_per_pdv() {
        return counter_per_pdv;
    }

    public void setCounter_per_pdv(int counter_per_pdv) {
        this.counter_per_pdv = counter_per_pdv;
    }

    public int getNumber_of_warehouse_per_pdv() {
        return number_of_warehouse_per_pdv;
    }

    public void setNumber_of_warehouse_per_pdv(int number_of_warehouse_per_pdv) {
        this.number_of_warehouse_per_pdv = number_of_warehouse_per_pdv;
    }

    public boolean isInventory_control() {
        return inventory_control;
    }

    public void setInventory_control(boolean inventory_control) {
        this.inventory_control = inventory_control;
    }

    public boolean isCombo_promotion_create() {
        return combo_promotion_create;
    }

    public void setCombo_promotion_create(boolean combo_promotion_create) {
        this.combo_promotion_create = combo_promotion_create;
    }

    public boolean isDashboard() {
        return dashboard;
    }

    public void setDashboard(boolean dashboard) {
        this.dashboard = dashboard;
    }

    public boolean isExport_report() {
        return export_report;
    }

    public void setExport_report(boolean export_report) {
        this.export_report = export_report;
    }

    public boolean isRole_andpermission_module() {
        return role_andpermission_module;
    }

    public void setRole_andpermission_module(boolean role_andpermission_module) {
        this.role_andpermission_module = role_andpermission_module;
    }

    public int getPoints_of_sale_number() {
        return points_of_sale_number;
    }

    public void setPoints_of_sale_number(int points_of_sale_number) {
        this.points_of_sale_number = points_of_sale_number;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getRequest_response_by() {
        return request_response_by;
    }

    public void setRequest_response_by(int request_response_by) {
        this.request_response_by = request_response_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "RequestReqPlanModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", client_name='" + client_name + '\'' +
                ", client_phone='" + client_phone + '\'' +
                ", client_address='" + client_address + '\'' +
                ", status_plan_request='" + status_plan_request + '\'' +
                ", type_plan='" + type_plan + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                ", status=" + status +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", bussines_number=" + bussines_number +
                ", user_number_per_pdv=" + user_number_per_pdv +
                ", cashier_number_per_pdv=" + cashier_number_per_pdv +
                ", supplies_inventory=" + supplies_inventory +
                ", multiple_closer_cashier=" + multiple_closer_cashier +
                ", counter_per_pdv=" + counter_per_pdv +
                ", number_of_warehouse_per_pdv=" + number_of_warehouse_per_pdv +
                ", inventory_control=" + inventory_control +
                ", combo_promotion_create=" + combo_promotion_create +
                ", dashboard=" + dashboard +
                ", export_report=" + export_report +
                ", role_andpermission_module=" + role_andpermission_module +
                ", points_of_sale_number=" + points_of_sale_number +
                ", created_by=" + created_by +
                ", request_response_by=" + request_response_by +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}
