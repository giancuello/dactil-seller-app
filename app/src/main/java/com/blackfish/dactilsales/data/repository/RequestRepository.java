package com.blackfish.dactilsales.data.repository;

import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.datasource.RequestDataSource;
import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.data.model.requestmodel.RequestPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;

public class RequestRepository {
    private static volatile RequestRepository instance;

    private RequestDataSource requestDataSource;

    private RequestRepository(RequestDataSource requestDataSource) {
        this.requestDataSource = requestDataSource;
    }

    public static RequestRepository getInstance(RequestDataSource requestDataSource){
        if (instance == null){
            instance = new RequestRepository(requestDataSource);
        }
        return instance;
    }

    public Result<ResponseRequestModelList> getListModel(){
        return requestDataSource.getList();
    }

    public Result<RequestPlanModel> createPlan(PlanModel planModel){
        return requestDataSource.createPlan(planModel);
    }
}
