package com.blackfish.dactilsales.data.interfaces;

import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCreateOwner;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseDetailPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseOwnerModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OwnerClient {
    @POST("comercial-owner-list-create")
    Call<ResponseCreateOwner> createOwner(@Body OwnerModel ownerModel);

    @GET("comercial-owner-list-create")
    Call<ResponseOwnerModel> getOwner();

    @GET("comercial-owner-list-create")
    Call<ResponseOwnerModel> searchOwner(@Query("search") String filter);

    @GET("comercial-owner-detail/{id_detail}")
    Call<ResponseDetailPlanModel> getDetail(@Path("id_detail") int id);
}
