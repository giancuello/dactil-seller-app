package com.blackfish.dactilsales.data.model.requestmodel;

import com.blackfish.dactilsales.data.model.CityModel;

import java.util.List;

public class RequestCityModel {
    private String message;
    private int code;
    private List<CityModel> data;
    private int draw;
    private int recordsTotal;
    private int recordsFiltered;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<CityModel> getData() {
        return data;
    }

    public void setData(List<CityModel> data) {
        this.data = data;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public RequestCityModel(String message, int code, List<CityModel> data, int draw, int recordsTotal, int recordsFiltered) {
        this.message = message;
        this.code = code;
        this.data = data;
        this.draw = draw;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
    }

    public RequestCityModel() {
    }

    @Override
    public String toString() {
        return "RequestCityModel{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", data=" + data +
                ", draw=" + draw +
                ", recordsTotal=" + recordsTotal +
                ", recordsFiltered=" + recordsFiltered +
                '}';
    }
}
