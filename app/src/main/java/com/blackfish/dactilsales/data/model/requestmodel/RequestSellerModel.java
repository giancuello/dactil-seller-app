package com.blackfish.dactilsales.data.model.requestmodel;

import com.blackfish.dactilsales.data.model.SellerModel;

public class RequestSellerModel {
    private String message;
    private int code;
    private SellerModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public SellerModel getData() {
        return data;
    }

    public void setData(SellerModel data) {
        this.data = data;
    }

    public RequestSellerModel(String message, int code, SellerModel data) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    public RequestSellerModel() {
    }

    @Override
    public String toString() {
        return "RequestSellerModel{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", data=" + data +
                '}';
    }
}
