package com.blackfish.dactilsales.data.model.responsemodel;

public class ResponseDetailPlanModel {
    private String message;
    private int code;
    private ResponsePlanDetailModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponsePlanDetailModel getData() {
        return data;
    }

    public void setData(ResponsePlanDetailModel data) {
        this.data = data;
    }
}
