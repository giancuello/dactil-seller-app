package com.blackfish.dactilsales.data.interfaces;

import com.blackfish.dactilsales.data.model.LoggedInUser;
import com.blackfish.dactilsales.data.model.SellerToken;
import com.blackfish.dactilsales.data.model.requestmodel.RequestSellerModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UserClient {
    @POST("token-seller")
    Call<SellerToken> loginUser(@Body LoggedInUser loggedInUser);

    @GET("get-seller-information")
    Call<RequestSellerModel> retrieveSellerInformation();
}
