package com.blackfish.dactilsales.data.repository;

import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.datasource.OwnerDataSource;
import com.blackfish.dactilsales.data.model.OwnerModel;
import com.blackfish.dactilsales.data.model.TypeDocumentModel;
import com.blackfish.dactilsales.data.model.ValidUser;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCityModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCountryModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseCreateOwner;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseDepartmentModelList;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseOwnerModel;

import java.util.List;

public class OwnerRepository {
    private static volatile OwnerRepository instance;
    private OwnerDataSource ownerDataSource;

    private OwnerRepository(OwnerDataSource ownerDataSource) {
        this.ownerDataSource = ownerDataSource;
    }

    public static OwnerRepository getInstance(OwnerDataSource ownerDataSource) {
        if (instance == null) {
            instance = new OwnerRepository(ownerDataSource);
        }
        return instance;
    }

    public Result<ResponseCreateOwner> createOwner(OwnerModel ownerModel) {
        return ownerDataSource.createOwner(ownerModel);
    }

    public Result<ResponseCityModelList> getCityList() {
        return ownerDataSource.getCityList();
    }

    public Result<ResponseCountryModelList> getCountryList() {
        return ownerDataSource.getCountryList();
    }

    public Result<ResponseDepartmentModelList> getDepartmentList() {
        return ownerDataSource.getDepartmentList();
    }

    public Result<List<TypeDocumentModel>> getTypeDocumentList() {
        return ownerDataSource.getTypeDocumentList();
    }

    public Result<ValidUser> validUsername(String search) {
        return ownerDataSource.validUsername(search);
    }
}
