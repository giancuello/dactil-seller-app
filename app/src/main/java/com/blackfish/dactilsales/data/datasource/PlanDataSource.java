package com.blackfish.dactilsales.data.datasource;

import android.util.Log;

import com.blackfish.dactilsales.consumer.ServiceGenerator;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.interfaces.PlansClient;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModel;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;

public class PlanDataSource {
    private ExecutorService executor
            = Executors.newSingleThreadExecutor();

    public Result<ResponseRequestModel> requestPlan(RequestReqPlanModel requestReqPlanModel, String token) {
        try {
            Future<ResponseRequestModel> requestModelFuture = makeNetworkRequestPlan(requestReqPlanModel, token);
            while (!requestModelFuture.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            ResponseRequestModel rrm = requestModelFuture.get();
            return new Result.Success<>(rrm);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public Result<ResponseRequestModel> requestUpdatePlan(RequestReqPlanModel requestReqPlanModel, String token) {
        try {
            Future<ResponseRequestModel> requestModelFuture = makeNetworkUpdateRequestPlan(requestReqPlanModel, token);
            while (!requestModelFuture.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            ResponseRequestModel rrm = requestModelFuture.get();
            return new Result.Success<>(rrm);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    private Future<ResponseRequestModel> makeNetworkRequestPlan(RequestReqPlanModel requestReqPlanModel, String token) {
        String makeToken = String.format("Bearer %s", token);
        PlansClient client = ServiceGenerator.createService(PlansClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseRequestModel> call = client.sendRequestPlan(requestReqPlanModel);
            return call.execute().body();
        });
    }

    private Future<ResponseRequestModel> makeNetworkUpdateRequestPlan(RequestReqPlanModel requestReqPlanModel, String token) {
        String makeToken = String.format("Bearer %s", token);
        PlansClient client = ServiceGenerator.createService(PlansClient.class, makeToken);
        return executor.submit(() -> {
            Call<ResponseRequestModel> call = client.updateRequestPlan(requestReqPlanModel, requestReqPlanModel.getId());
            return call.execute().body();
        });
    }
}
