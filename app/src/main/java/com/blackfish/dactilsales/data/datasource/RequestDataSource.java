package com.blackfish.dactilsales.data.datasource;

import android.util.Log;

import com.blackfish.dactilsales.consumer.ServiceGenerator;
import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.interfaces.PlansClient;
import com.blackfish.dactilsales.data.model.PlanModel;
import com.blackfish.dactilsales.data.model.requestmodel.RequestPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModelList;
import com.blackfish.dactilsales.util.Common;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;

public class RequestDataSource {
    private ExecutorService executor
            = Executors.newSingleThreadExecutor();

    public Result<ResponseRequestModelList> getList(){
        try{
            Future<ResponseRequestModelList> requestModelFuture = getModelListAuth();
            while (!requestModelFuture.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            ResponseRequestModelList rrm = requestModelFuture.get();
            return new Result.Success<>(rrm);
        }catch (Exception e){
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error to request Response to Plan List", e));
        }
    }

    public Result<RequestPlanModel> createPlan(PlanModel planModel){
        try{
            Future<RequestPlanModel> requestModelFuture = createPlanNetworkRequest(planModel);
            while (!requestModelFuture.isDone()){
                System.out.println("Requesting...");
                Thread.sleep(300);
            }
            RequestPlanModel rrm = requestModelFuture.get();
            return new Result.Success<>(rrm);
        }catch (Exception e){
            Log.e(getClass().getName(), e.getMessage(), e);
            return new Result.Error(new IOException("Error to request Response to Plan List", e));
        }
    }

    private Future<ResponseRequestModelList> getModelListAuth() {
        String token = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        PlansClient plansClient = ServiceGenerator.createService(PlansClient.class, token);
        return executor.submit(() -> {
            Call<ResponseRequestModelList> call = plansClient.getRequestPlans();
            return call.execute().body();
        });
    }

    private Future<RequestPlanModel> createPlanNetworkRequest(PlanModel planModel){
        String token = String.format("Bearer %s", Common.CURRENT_SELLER.getData());
        PlansClient client = ServiceGenerator.createService(PlansClient.class, token);
        return executor.submit(() -> {
            Call<RequestPlanModel> call = client.createPlan(planModel);
            return call.execute().body();
        });
    }
}
