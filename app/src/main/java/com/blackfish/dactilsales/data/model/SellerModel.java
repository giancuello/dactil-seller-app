package com.blackfish.dactilsales.data.model;

public class SellerModel {
    private int id;
    private String name;
    private String type_user;
    private String type_seller;
    private String first_surname;
    private String second_surname;
    private boolean status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType_user() {
        return type_user;
    }

    public void setType_user(String type_user) {
        this.type_user = type_user;
    }

    public String getType_seller() {
        return type_seller;
    }

    public void setType_seller(String type_seller) {
        this.type_seller = type_seller;
    }

    public String getFirst_surname() {
        return first_surname;
    }

    public void setFirst_surname(String first_surname) {
        this.first_surname = first_surname;
    }

    public String getSecond_surname() {
        return second_surname;
    }

    public void setSecond_surname(String second_surname) {
        this.second_surname = second_surname;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public SellerModel(int id, String name, String type_user, String type_seller, String first_surname, String second_surname, boolean status) {
        this.id = id;
        this.name = name;
        this.type_user = type_user;
        this.type_seller = type_seller;
        this.first_surname = first_surname;
        this.second_surname = second_surname;
        this.status = status;
    }

    public SellerModel() {
    }

    @Override
    public String toString() {
        return "SellerModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type_user='" + type_user + '\'' +
                ", type_seller='" + type_seller + '\'' +
                ", first_surname='" + first_surname + '\'' +
                ", second_surname='" + second_surname + '\'' +
                ", status=" + status +
                '}';
    }
}
