package com.blackfish.dactilsales.data.repository;

import com.blackfish.dactilsales.data.Result;
import com.blackfish.dactilsales.data.datasource.PlanDataSource;
import com.blackfish.dactilsales.data.model.RequestReqPlanModel;
import com.blackfish.dactilsales.data.model.responsemodel.ResponseRequestModel;

public class NewRequestRepository {

    private static volatile NewRequestRepository instance;
    private PlanDataSource planDataSource;

    private RequestReqPlanModel requestReqPlanModel;

    private NewRequestRepository(PlanDataSource planDataSource) {
        this.planDataSource = planDataSource;
    }

    public static NewRequestRepository getInstance(PlanDataSource planDataSource){
        if (instance == null){
            instance = new NewRequestRepository(planDataSource);
        }
        return instance;
    }

    public void setRequestReqPlanModel(RequestReqPlanModel requestReqPlanModel){
        this.requestReqPlanModel = requestReqPlanModel;
    }

    public Result<ResponseRequestModel> requestPlan(RequestReqPlanModel requestReqPlanModel, String token) {
        // handle login
        Result<ResponseRequestModel> result = planDataSource.requestPlan(requestReqPlanModel, token);
        if (result instanceof Result.Success) {
            ResponseRequestModel pm = ((Result.Success<ResponseRequestModel>) result).getData();
            if (pm.getData() != null){
                setRequestReqPlanModel(pm.getData());
            }
        }
        return result;
    }

    public Result<ResponseRequestModel> updateRequestPlan(RequestReqPlanModel requestReqPlanModel, String token) {
        // handle login
        Result<ResponseRequestModel> result = planDataSource.requestUpdatePlan(requestReqPlanModel, token);
        if (result instanceof Result.Success) {
            ResponseRequestModel pm = ((Result.Success<ResponseRequestModel>) result).getData();
            if (pm.getData() != null){
                setRequestReqPlanModel(pm.getData());
            }
        }
        return result;
    }

}
